import React from "react";
import rolling from '../../assets/Rolling.svg';
import './styles.scss';

export default function Preloader() {
    return (
        <div id="loader-wrapper">
            <div className="loader">
                <img src={rolling} alt="preloader"/>
            </div>
        </div>
    );
}
