import React from 'react';
import { Link } from 'react-router-dom';

export default function Snackbar ({ message, link }) {
    return (
        <div className="success_popup">
            <p className="m-0">{ message }</p>
            {link && <Link to={ link.url } className="font-weight-bold m-0">{ link.text }</Link>}
        </div>
    );
}