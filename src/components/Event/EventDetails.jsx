import React from "react";
import StackAvatar from "../StackAvatar";
import { format } from "date-fns";
import { BsWatch, BsFillCalendarCheckFill } from "react-icons/bs";
import { IN, OUT } from "../../constants";

export default function EventDetails({
    event,
    handleAttendeesAction,
    playersIn,
    playersOut,
    disableBtn,
    eventDetails,
}) {
    const disableIfPaiementExists = () => {
        eventDetails.forEach((event) => {
            console.log(event);
            if (event.invoice_amount !== null) return false;
            else return true;
        });
    };

    return (
        <div className="col-lg-7">
            <div className="card shadow-none">
                <div className="">
                    <img
                        src="https://images.hdqwalls.com/wallpapers/basketball-court-4k-p9.jpg"
                        alt=""
                        className="card-img-top"
                        style={{
                            borderRadius: "20px",
                            maxHeight: "300px",
                            objectFit: "cover",
                        }}
                    />
                </div>
                <div className="d-flex justify-content-between  align-items-center mt-2 py-2 px-2">
                    <div className="d-flex align-items-center font-weight-bold">
                        <BsFillCalendarCheckFill className="h5 text-grey" />
                        <span className="ml-2">
                            {event && format(new Date(event.venue), "dd MMM")}
                        </span>
                    </div>
                    <div className="d-flex align-items-center font-weight-bold">
                        <BsWatch className="h5 text-grey" />
                        <span className="ml-2">
                            {event && event.time.substring(0, 5)}
                        </span>
                    </div>
                </div>

                <div className="card-body px-0 pt-2">
                    <h2 className="mb-3 font-weight-bold mb-5">{event.name}</h2>

                    <div className="d-flex justify-content-between align-items-center mt-3">
                        <p className="font-weight-bold">
                            {playersIn ? (
                                <span className="badge badge-primary badge-pill py-2 px-3">
                                    {playersIn.length}
                                </span>
                            ) : (
                                "0"
                            )}{" "}
                            inscrit
                            {playersIn.length > 1 ? "s" : ""}
                        </p>
                        <div className="d-flex align-items-center ">
                            {playersIn.length >= 1 ? (
                                <StackAvatar
                                    players={playersIn}
                                    avatarsToDisplay={4}
                                />
                            ) : (
                                ""
                            )}
                        </div>
                    </div>

                    <div className="d-flex justify-content-between justify-content-sm-start align-items-center mt-2 py-2 rounded-pill">
                        <button
                            onClick={(e) => handleAttendeesAction(e, IN)}
                            className="btn ripple btn-primary rounded-pill mr-sm-4"
                            disabled={disableBtn(playersIn)}
                        >
                            {!disableBtn(playersOut)
                                ? "Vous êtes inscrits"
                                : "Je serais présent"}
                        </button>

                        {eventDetails.length === 0 &&
                        !disableIfPaiementExists() ? (
                            <button
                                onClick={(e) => handleAttendeesAction(e, OUT)}
                                className="btn ripple btn-outline-black rounded-pill"
                                disabled={disableBtn(playersOut)}
                            >
                                Je serais absent
                            </button>
                        ) : (
                            ""
                        )}
                    </div>

                    <div className="mt-3">
                        <h5 className="mb-3 font-weight-bold">Mot du coach</h5>
                        <p className="">{event.description}</p>
                    </div>
                </div>
            </div>
        </div>
    );
}
