import React from "react";
import ModalWrapper from "../Modals/ModalWrapper";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import TextInput from "../Forms/TextInput";
import { useDispatch } from "react-redux";
import { closeModal } from "../Modals/modalReducer";
import { addContribution } from "../../services/event";
import { useHistory } from "react-router-dom";


export default function EvenTaskItemModal({user, event, type, teamId}) {
    const dispatch = useDispatch();
    const history = useHistory();
    const initialValues = {
        invoice_amount: '',        
    };

    const validationSchema = Yup.object({
        invoice_amount: Yup.number().nullable(),
    });

    const addPlayerContribution = (values) => {
        const req = {    
            team_id: teamId,       
            type: type,
            invoice_amount: values.invoice_amount
        }       
        addContribution(event, req).then(res => {
            if(res.status === 204) {
                console.log(res.data.data);
                dispatch(closeModal());
                history.push(`/event/show/${event}`);
            }
        })
    }

    return (
        <ModalWrapper header="Justifier mes dépenses" backdrop={true}>
            <div className="py-2">                
                <Formik
                    initialValues={initialValues}
                    onSubmit={(values, { setFieldError }) => {
                        addPlayerContribution(values, setFieldError);                        
                    }}
                    validationSchema={validationSchema}
                >
                    {(formik) => {
                        const { isValid, dirty } = formik;
                        return (
                            <Form>
                                <TextInput
                                    label="Montant facture"
                                    name="invoice_amount"
                                    type="number"
                                    min="1"
                                    max="150"
                                />
                               
                                <div className="d-flex align-items-center float-right mt-4">
                                    <button
                                        type="submit"
                                        className="btn btn-primary rounded-pill shadow-none"
                                        disabled={!(isValid && dirty)}
                                    >
                                        Enregistrer
                                    </button>
                                    <button
                                        onClick={() => dispatch(closeModal())}
                                        className="btn btn-light rounded-pill ml-3"
                                    >
                                        Annuler
                                    </button>
                                </div>
                            </Form>
                        );
                    }}
                </Formik>
            </div>
        </ModalWrapper>
    );
}
