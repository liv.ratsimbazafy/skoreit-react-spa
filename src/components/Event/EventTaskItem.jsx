import React from "react";
import StackAvatar from "../StackAvatar";
import { useDispatch } from "react-redux";
import { openModal } from "../Modals/modalReducer";
import format from "date-fns/format";
import { MdArrowForward } from "react-icons/md";
import { ADD, REMOVE } from "../../constants";

export default function EventTaskItem({
    players,
    connectedUser,
    subTitle,
    handleEventTasks,
    type,
    classes,
    event,
    eventDetails,
    team,
    disableBtn,
    playersOut,
}) {
    const dispatch = useDispatch();
    let doHaveTask = players.find((player) => player.id === connectedUser.id);
    let isDrink = eventDetails.find((event) => event.buy_drinks !== 0);
    let isMeal = eventDetails.find((event) => event.buy_meals !== 0);

    const HandleTaskItemBtn = () => {
        return players.find((player) => player.id === connectedUser.id) ? (
            subTitle.includes("boisson") &&
            isDrink &&
            isDrink.invoice_amount ? (
                <PaidInvoiceBadge
                    amount={isDrink.invoice_amount}
                    date={isDrink.updated_at}
                />
            ) : subTitle.includes("repas") &&
              isMeal &&
              isMeal.invoice_amount ? (
                <PaidInvoiceBadge
                    amount={isMeal.invoice_amount}
                    date={isMeal.updated_at}
                />
            ) : (
                <button
                    onClick={(e) => handleEventTasks(e, type, REMOVE)}
                    className="btn btn-outline-dark rounded-pill"
                >
                    Je change d'avis
                </button>
            )
        ) : (
            <button
                onClick={(e) => handleEventTasks(e, type, ADD)}
                className="btn btn-primary rounded-pill"
                disabled={disableBtn(playersOut)}
            >
                Je m'en occupe
            </button>
        );
    };

    const PaidInvoiceBadge = ({ amount, date }) => {
        return (
            <p>
                <span className="text-success font-weight-bold">{amount}€</span>{" "}
                paié le{" "}
                <span className="badge badge-success text-white badge-pill py-2">
                    {" "}
                    {format(new Date(date), "dd MMM")}
                </span>
            </p>
        );
    };

    const AddInvoiceBtn = () => {
        return (
            <div className="d-flex justify-content-end align-items-center mt-2">
                <small
                    onClick={() =>
                        dispatch(
                            openModal({
                                modalType: "EvenTaskItemModal",
                                modalProps: {
                                    user: connectedUser.id,
                                    event: event.id,
                                    type: type,
                                    teamId: team.team_id,
                                },
                            })
                        )
                    }
                    style={{ cursor: "pointer" }}
                    className="text-primary font-weight-bold"
                >
                    Ajouter mes justificatifs{" "}
                    <MdArrowForward className="ml-2" />
                </small>
            </div>
        );
    };

    const PaidInvoiceDetails = () => {
        return (
            <div className="d-flex justify-content-end align-items-center">
                <small
                    className="text-primary font-weight-bold"
                    style={{ cursor: "pointer" }}
                >
                    voir les détails <MdArrowForward className="ml-3" />
                </small>
            </div>
        );
    };

    const DisplayEventDetails = () => {
        let item;

        if (doHaveTask && isDrink && subTitle.includes("boisson")) {
            eventDetails.map((ev) => {
                if (isDrink.invoice_amount)
                    item = <PaidInvoiceDetails key={ev.id} item={isDrink} />;
                else item = <AddInvoiceBtn />;
                return item;
            });
        } else if (doHaveTask && isMeal && subTitle.includes("repas")) {
            eventDetails.map((ev) => {
                if (isMeal.invoice_amount)
                    item = <PaidInvoiceDetails key={ev.id} item={isMeal} />;
                else item = item = <AddInvoiceBtn />;
                return item;
            });
        } else {
            item = "";
        }

        return item;
    };

    return (
        <div className={classes}>
            <h5 className="font-weight-bold mb-3">{subTitle}</h5>

            <div className="d-flex justify-content-between align-items-center">
                <div className="d-flex align-items-center">
                    {players.length >= 1 ? (
                        <StackAvatar players={players} avatarsToDisplay={2} />
                    ) : (
                        <small className="text-primary_l_600 font-weight-bold">Aucun responsable !</small>
                    )}
                </div>
                <div className="d-flex justify-content-between align-items-center">
                    <HandleTaskItemBtn />
                </div>
            </div>

            <DisplayEventDetails />
        </div>
    );
}
