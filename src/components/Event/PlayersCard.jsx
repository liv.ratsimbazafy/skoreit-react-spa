import React from "react";

export default function PlayersCard({...props}) {
    return (
        <div
            className="card shadow-sm"
            style={{ minHeight: "15rem", borderRadius: "20px" }}
        >            
            <div className="card-body">
                {props.data && props.data.length
                    ? props.data.map(item => {
                          return (
                            <div key={item.id} className="d-flex  align-items-center mb-2" >
                                <img src={item.profile_img} alt="" className="user_profile_img" />
                            <span className="font-weight-bold ml-2">{item.name}</span>
                        </div>
                          );
                      })
                    : "Aucun joueur inscrit !"}
            </div>
        </div>
    );
}
