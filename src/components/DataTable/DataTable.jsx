import React from "react";
import { BsThreeDots, BsTrash, BsEye } from "react-icons/bs";
import { BiEditAlt } from "react-icons/bi";
import { Link } from "react-router-dom";
import { format } from "date-fns";
import "./styles.scss";

export default function DataTable({
    colTitles,
    data,
    colValues,
    isDate,
    url,
    ...props
}) {
    let ids = [];
    const tableStyles = props.fixed
        ? { minHeight: props.fixed.height + "px" }
        : null;
    const loader = props.isLoading ? "loading" : "";

    return (
        <div className="table-responsive mt-3 bg-light" style={tableStyles}>
            <table className={`table table-hover ${loader}`}>
                <thead>
                    <tr>
                        {colTitles["id"] ? <th scope="col">#</th> : null}
                        {colTitles.map((title) => {
                            return (
                                <th scope="col" key={title}>
                                    {title}
                                </th>
                            );
                        })}
                    </tr>
                </thead>
                <tbody>
                    {data
                        ? data.map((item, i) => {
                              if (Object.keys(item).includes("id"))
                                  ids.push(item.id);
                              return (
                                  <tr key={i}>
                                      {colValues.map((value, index) => {
                                          return (
                                              <td
                                                  key={index}
                                                  className="text-dark font-weight-bold"
                                              >
                                                  {isDate &&
                                                  value === "venue" ? (
                                                      format(
                                                          new Date(item[value]),
                                                          "dd LLL"
                                                      )
                                                  ) : value ===
                                                    "email_verified_at" ? (
                                                      item[value] === null ? (
                                                          "ko"
                                                      ) : (
                                                          "ok"
                                                      )
                                                  ) : value === "name" ? (
                                                      <span className="text-capitalize">
                                                          {item[value]}
                                                      </span>
                                                  ) : (
                                                      <span>{item[value]}</span>
                                                  )}
                                              </td>
                                          );
                                      })}
                                      <td>
                                          <div
                                              className="player_dropdown"
                                              id="dd"
                                          >
                                              <span
                                                  className="drop_icon"
                                                  data-toggle={"dropdown"}
                                                  data-boundary="dd"
                                              >
                                                  <BsThreeDots />
                                              </span>
                                              <div
                                                  className="dropdown-menu shadow"
                                                  id="table_dropdown"
                                              >
                                                  <Link
                                                      to={`/${url}/show/${ids[i]}`}
                                                      className="dropdown-item"
                                                  >
                                                      <BsEye className=" player-i" />
                                                      <span className="align-middle ml-3">
                                                          Détails
                                                      </span>
                                                  </Link>
                                                  <Link
                                                      to={`/${url}/admin/show/${ids[i]}`}
                                                      className="dropdown-item"
                                                  >
                                                      <BsEye className=" player-i" />
                                                      <span className="align-middle ml-3">
                                                          Admin
                                                      </span>
                                                  </Link>
                                                  <Link
                                                      to={`/${url}/edit/${ids[i]}`}
                                                      className="dropdown-item"
                                                  >
                                                      <BiEditAlt className=" player-i" />
                                                      <span className="align-middle ml-3">
                                                          Modifier
                                                      </span>
                                                  </Link>
                                                  <p
                                                      className="dropdown-item m-0"
                                                      onClick={(e) =>
                                                          props.deleteResource(
                                                              e,
                                                              ids[i]
                                                          )
                                                      }
                                                  >
                                                      <BsTrash className="player-i" />
                                                      <span className="align-middle ml-3">
                                                          Supprimer
                                                      </span>
                                                  </p>
                                              </div>
                                          </div>
                                      </td>
                                  </tr>
                              );
                          })
                        : null}
                </tbody>
            </table>
        </div>
    );
}
