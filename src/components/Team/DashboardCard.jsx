import React from 'react';
import './styles.scss';

export default function DashboardCard({icon, title, value, bg}) {
    return (
        <div className="col-sm-4 mb-3 mb-lg-0">
            <div className={`card text-center rounded-lg ${bg} shadow-sm`} id="dashboard_card">
                <div className="header m-0">
                    <span className="h3 bg-gradient-primary_l_700">
                        { icon }
                    </span>
                    <p className="text-capitalize mt-n2 font-weight-bold text-white">{title}</p>
                </div>
                <p className="h1 mt-4 font-weight-normal text-white">{ value }</p>
            </div>
        </div>
    )
}