import React from 'react';
import { Link } from 'react-router-dom';
import { formatDate } from '../../helpers';


export default function TeamEventCard({ events }) {
    const {upcoming_events, passed_events} = events;    
    return (
        <section className="pb-5">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h4 className="mb-3 font-weight-bold">Mes Évènements</h4>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6 mb-3 mb-md-0">
                        <div className="card" style={{ minHeight: '20rem', borderRadius: '20px' }}>
                            <div className="card-header bg-transparent">
                                <h5>Évènement de la semaine</h5>
                            </div>
                            <div className="card-body">

                                { upcoming_events && upcoming_events.length
                                    ?
                                    upcoming_events.map(event => {
                                        return <Link
                                            to={`/event/show/${event.id}`}
                                            key={event.id}
                                            className="nav-link">
                                            {formatDate(event.venue)} | {event.time.substr(0,5)} | {event.name}
                                        </Link>
                                    })

                                    : 'Pas d\'évènement à venir !'}
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="card" style={{ minHeight: '20rem', borderRadius: '20px' }}>
                            <div className="card-header bg-transparent">
                                <h5>Évènement Passés</h5>
                            </div>
                            <div className="card-body">

                                { passed_events && passed_events.length
                                    ?
                                    passed_events.map(event => {
                                        return <Link
                                            to={`/event/show/${event.id}`}
                                            key={event.id}
                                            className="nav-link">
                                            {formatDate(event.venue)} | {event.time.substr(0,5)} | {event.name}
                                        </Link>
                                    })

                                    : 'Pas d\'évènement !'}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}