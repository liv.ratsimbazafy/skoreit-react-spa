import React from "react";
import DashboardCard from "./DashboardCard";
//import { IoStatsChartSharp } from "react-icons/io5";
import { BsCurrencyEuro, BsCartCheck } from "react-icons/bs";
import CircularProgress from "./CircularProgress";

export default function TeamStat({ userData }) {
    return (
        <section className="pb-5" style={{marginTop: '3rem'}}>
            <div className="container">
                <div className="row">
                    {/* <div className="col-md-12">
                        <h4 className="mb-3 font-weight-bold">Mes stats</h4>
                    </div> */}
                    <CircularProgress value={userData.attendance_rate}/>
                    {/* <DashboardCard
                        title="Présence"
                        icon={<IoStatsChartSharp />}
                        value={userData.attendance_rate + "%"}
                        bg="bg-gradient-secondary1"
                    /> */}
                    <DashboardCard
                        title="Cotisation"
                        icon={<BsCurrencyEuro className="text-secondary"/>}
                        value={userData.contribution_unpaid + "€"}
                        bg="bg-gradient-secondary"
                    />
                    <DashboardCard
                        title="Courses"
                        icon={<BsCartCheck className="text-accent3"/>}
                        value={userData.contribution_paid + "€"}
                        //bg="bg-gradient-primary_l_600"
                        bg="bg-gradient-accent3"
                    />
                </div>
            </div>
        </section>
    );
}
