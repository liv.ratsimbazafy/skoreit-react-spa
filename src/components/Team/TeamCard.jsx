import React from "react";
import { Link } from "react-router-dom";
import { format } from "date-fns";
import { MdOutlineLocationOn } from "react-icons/md";

import "./styles.scss";

export default function TeamCard({ team }) {
    return (
        <section className="pb-5">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h4 className="mb-3 font-weight-bold text-black">
                            Mon équipe
                        </h4>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6 mb-3 mb-md-0">
                        <div
                            className="card card-bgi rounded-lg"
                            style={{
                                backgroundImage: `url("https://wallpaperaccess.com/thumb/7175922.jpg")`,
                            }}
                        >
                            <div className="card-bgi-box text-capitalize">
                                <h1 className="text-capitalize ">
                                    {team.name}
                                </h1>
                                <h5>
                                    {team.sport} - {team.category}
                                </h5>
                                <div className=" d-flex justify-content-center align-items-center text-white mt-2 ">
                                    <MdOutlineLocationOn className="h5 font-weight-bold" />
                                    <small className="text-white mb-1 font-weight-bold">
                                        {team.city}
                                    </small>
                                </div>
                                <div className="mt-3 text-justify px-5">
                                    <small>{team.description}</small>
                                </div>
                                <Link
                                    to={`/team/${team.id}/settings`}
                                    className="btn btn-outline-light mt-2 rounded-pill"
                                >
                                    Paramètres
                                </Link>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-6">
                        <div className="card h-min shadow rounded-lg">
                            <div className="card-header">
                                <h5 className="font-weight-bold">À propos</h5>
                            </div>
                            <div className="card-body">
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item d-flex justify-content-between align-items-center">
                                        <small className="text-dark font-weight-bold">Saison</small>
                                        <small className="text-dark font-weight-bold">
                                            {" "}
                                            {team.season_start.substr(0, 4)}
                                        </small>
                                    </li>
                                    <li className="list-group-item d-flex justify-content-between align-items-center ">
                                        <small className="text-dark font-weight-bold">Inscrit le</small>
                                        <small className="text-dark font-weight-bold">
                                            {" "}
                                            {format(
                                                new Date(team.created_at),
                                                "dd MMM yyyy"
                                            )}
                                        </small>
                                    </li>
                                    <li className="list-group-item d-flex justify-content-between align-items-center border-bottom">
                                        <small className="text-dark font-weight-bold">Coach - Organisateur</small>
                                        <div className="d-flex justify-content-between align-items-center">
                                            <img
                                                src={team.coach.profile_img}
                                                alt="user_profile"
                                                style={{
                                                    width: "45px",
                                                    height: "45px",
                                                    objectFit: "cover",
                                                    borderRadius: "50%",
                                                }}
                                            />
                                            <p className="ml-2">
                                                {team.coach.name}
                                            </p>
                                        </div>
                                    </li>
                                </ul>
                                <div className="d-flex justify-content-between align-items-center mt-4">
                                    <div className="d-flex justify-content-between align-items-center px-md-2">
                                        <small className="font-weight-bold text-dark">Nbr joueurs </small>
                                        <span className="bg-primary_l_600 text-primary px-2 rounded ml-1 font-weight-bold">
                                            {team.players &&
                                                team.players.length}
                                        </span>
                                    </div>
                                    <Link
                                        to="/players"
                                        className="btn btn-primary_l_700 text-primary shadow-none"
                                    >
                                        Voir les joueurs
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
