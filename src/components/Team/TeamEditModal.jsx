import React, { useState, useEffect } from "react";
import ModalWrapper from "../Modals/ModalWrapper";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import TextInput from "../Forms/TextInput";
import SelectInput from "../Forms/SelectInput";
import { typesAndCategories, team_edit } from "../../services/team";
import { useDispatch } from "react-redux";
import { closeModal } from "../Modals/modalReducer";
import TextAreaInput from "../Forms/TextAreaInput";
import { useHistory } from "react-router-dom";

export default function TeamEditModal({ team }) {
    const dispatch = useDispatch();
    const [sports, setSports] = useState([]);
    const [categories, setCategories] = useState([]);
    const history = useHistory();

    useEffect(() => {
        let isMounted = true;
        if (isMounted) getSports();
        return (isMounted = false);
    }, []);

    function getSports() {
        typesAndCategories().then((res) => {
            if (res.status === 200) {
                setSports(res.data.data.sports);
                setCategories(res.data.data.categories);
            }
        });
    }

    const initialValues = {
        name: team.name,
        types_id: team && team.sport,
        categories_id: team && team.category,
        description: team.description || " ",
        city: team ? team.city : " ",
    };

    const validationSchema = Yup.object({
        name: Yup.string()
            .max(100, "100 caractères maximum !")
            .required("Nom de l'équipe obligatoire."),
        types_id: Yup.string().required("Sport obligatoire."),
        categories_id: Yup.string().required("Catégorie obligatoire."),
        description: Yup.string()
            .max(200, "200 caractères maximum !")
            .nullable(),
        city: Yup.string().max(200, "200 caractères maximum !").nullable(),
    });

    function mapper(maps, cat) {
        let id;
        maps.forEach((item) => {
            if (item.name === cat) {
                id = item.id;
            }
        });
        return id;
    }

    const updateTeamData = (values) => {
        const real_values = {
            name: values.name,
            description: values.description,
            city: values.city,
            types_id:
                values.types_id.length > 1
                    ? mapper(sports, values.types_id)
                    : values.types_id,
            categories_id:
                values.categories_id.length > 1
                    ? mapper(categories, values.categories_id)
                    : values.categories_id,
        };
        team_edit(team.id, real_values).then((res) => {
            if (res.status === 204) {
                history.push(`/team/${team.id}/settings`);
                dispatch(closeModal());
            }
        });
    };
    return (
        <ModalWrapper header="Modifier" backdrop="static">
            <div className="py-2">
                <Formik
                    initialValues={initialValues}
                    onSubmit={(values, { setFieldError }) => {
                        updateTeamData(values, setFieldError);
                    }}
                    validationSchema={validationSchema}
                >
                    {(formik) => {
                        const { isValid, dirty } = formik;
                        return (
                            <Form>
                                <TextInput
                                    label="Nom de l'équipe"
                                    name="name"
                                />
                                <div className="form-row">
                                    <div className="col-6">
                                        <SelectInput
                                            label="Choisir le sport"
                                            name="types_id"
                                            options={sports}
                                            defaultOption={team && team.sport}
                                        />
                                    </div>
                                    <div className="col-6">
                                        <SelectInput
                                            label="Choisir la catégorie"
                                            name="categories_id"
                                            options={categories}
                                            defaultOption={
                                                team && team.category
                                            }
                                        />
                                    </div>
                                </div>
                                <TextInput label="Ville" name="city" />
                                <TextAreaInput
                                    label="Description"
                                    name="description"
                                    rows="4"
                                />

                                <div className="d-flex align-items-center float-right mt-4">
                                    <button
                                        type="submit"
                                        className="btn btn-primary rounded-pill"
                                        disabled={!(isValid && dirty)}
                                    >
                                        Modifier
                                    </button>
                                    <button
                                        onClick={() => dispatch(closeModal())}
                                        className="btn btn-light rounded-pill ml-3"
                                    >
                                        Annuler
                                    </button>
                                </div>
                            </Form>
                        );
                    }}
                </Formik>
            </div>
        </ModalWrapper>
    );
}
