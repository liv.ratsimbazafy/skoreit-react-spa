import React, { useEffect } from "react";
import ModalWrapper from "../Modals/ModalWrapper";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import TextInput from "../Forms/TextInput";
import { useDispatch } from "react-redux";
import { closeModal } from "../Modals/modalReducer";
import { useHistory } from "react-router-dom";
import { teamUpdateContributionPrice } from "../../services/team";

export default function TeamContributionModal({ team }) {
    const dispatch = useDispatch();

    const history = useHistory();

    useEffect(() => {}, []);

    const initialValues = {
        contribution_price: team && team.team_details.contribution_price,
    };

    const validationSchema = Yup.object({
        contribution_price: Yup.number().integer().required("Obligatoire."),
    });

    const updateContribution = ({ contribution_price }) => {
        const data = {
            team_id: team.id,
            contribution_price: contribution_price,
        };
        teamUpdateContributionPrice(data).then((res) => {
            if (res.status === 204) {
                history.push(`/team/${team.id}/settings`);
                dispatch(closeModal());
            }
        });
    };

    return (
        <ModalWrapper
            header="Montant de la participation (€)"
            backdrop={true}
            size="sm"
        >
            <div className="py-2">
                <Formik
                    initialValues={initialValues}
                    onSubmit={(values, { setFieldError }) => {
                        updateContribution(values, setFieldError);
                    }}
                    validationSchema={validationSchema}
                >
                    {(formik) => {
                        const { isValid, dirty } = formik;
                        return (
                            <Form>
                                <TextInput
                                    name="contribution_price"
                                    type="number"
                                    min="0"
                                    max="50"
                                    row="row"
                                />
                                <div className="d-flex align-items-center float-right mt-4">
                                    <button
                                        type="submit"
                                        className="btn btn-primary rounded-pill"
                                        disabled={!(isValid && dirty)}
                                    >
                                        Modifier
                                    </button>
                                    <button
                                        onClick={() => dispatch(closeModal())}
                                        className="btn btn-light rounded-pill ml-3"
                                    >
                                        Annuler
                                    </button>
                                </div>
                            </Form>
                        );
                    }}
                </Formik>
            </div>
        </ModalWrapper>
    );
}
