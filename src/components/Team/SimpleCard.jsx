import React from "react";

export default function SimpleCard(props) {
    return (
        <div className="card h-min rounded-lg shadow">
            <div className="card-header">
                <h5 className="font-weight-bold">
                    {props.total && (
                        <span className="text-light bg-info py-1 px-3 rounded-lg">
                            {props.total}
                        </span>
                    )}{" "}
                    {props.header}
                </h5>
            </div>
            <div className="card-body">{props.children}</div>
        </div>
    );
}
