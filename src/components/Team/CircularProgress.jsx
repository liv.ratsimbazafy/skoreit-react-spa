import React from "react";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
//import ChangingProgressProvider from "./ChangingProgressProvider";

export default function CircularProgress({value}) {
    const percentage = value;
  
    return (
        <div className="col-sm-4 mb-3 mb-lg-0">
            <div
                className="d-flex justify-content-center align-items-center p-3"
                style={{ minHeight: 200 }}
            >
                <div style={{ width: 130 }}>
                    <h4 className="mb-3 text-center font-weight-bold text-dark">Présences</h4>
                    <CircularProgressbar
                        value={percentage}
                        text={`${percentage}%`}
                        background
                        backgroundPadding={6}
                        styles={buildStyles({
                            backgroundColor: "#f4eefe",
                            textColor: "#4A4453",
                            textSize: '20px',
                            pathColor: "#9C6CFE",
                            trailColor: "transparent"
                          })}
                    />
                    {/* <ChangingProgressProvider values={[0, 20, 40, 60, 80, 100]}>
                        {percentage => (
                        <CircularProgressbar value={percentage} text={`${percentage}%`} />
                        )}
                    </ChangingProgressProvider> */}
                </div>
                
            </div>
        </div>
    );
}
