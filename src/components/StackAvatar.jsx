import React from "react";

export default function StackAvatar({ players, avatarsToDisplay }) {
    const outlinedStyles = {
        width: "45px",
        height: "45px",
        objectFit: "cover",
        borderRadius: "50%",
        marginLeft: "-5px",
        outline: "3px solid white",
    };

    const normalStyles = {
        width: "45px",
        height: "45px",
        objectFit: "cover",
        borderRadius: "50%",
        marginRight: "5px",
        boxShadow: "5px 5px 5px #E8E8E8"
    };

    const switchAvatars = (users, items) => {
        return users.length > 2
            ? users
                  .slice(0, items)
                  .map((item, id) => (
                      <img
                          key={id}
                          src={item}   
                          alt={item}                       
                          style={outlinedStyles}
                      />
                  ))
            : users.map((item, id) => (
                  <img key={id} src={item} alt={item} style={normalStyles} />
              ));
    };

    let avatarsArray = [];    
    let totalPlayers = players.length;
    avatarsArray = players.map((player) => player.profile_img);   
    return (
        <>
            {switchAvatars(avatarsArray, avatarsToDisplay)}
            {totalPlayers > avatarsToDisplay && (
                <span
                    style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        width: "40px",
                        height: "40px",
                        background: "#f7f7f7",
                        borderRadius: "50px",
                        marginLeft: "-7px",
                        outline: "3px solid white",
                    }}
                >
                    + {totalPlayers - avatarsToDisplay}
                </span>
            )}
        </>
    );
}
