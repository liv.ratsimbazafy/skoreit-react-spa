import React, { Fragment } from "react";
import { AiOutlinePlus } from "react-icons/ai";
import { useHistory } from "react-router";
import ReactTooltip from "react-tooltip";
import "./styles.scss";

export default function IconAdd({ ...props }) {
    const history = useHistory();

    const redirectTo = (e, history, path) => {
        e.preventDefault();
        history.push(path);
    };
    return (
        <Fragment>
            <span
                onClick={
                    props.asLink
                        ? (e) => redirectTo(e, history, props.path)
                        : (e) => props.onClick(e)
                }
                //className="add_icon ripple"
                className={`add_icon ${props.customClass}`}
                data-tip
                data-for={props.title}
            >
                <AiOutlinePlus />
            </span>

            <ReactTooltip
                id={props.title}
                place={props.place ?? "bottom"}
                effect="solid"
                backgroundColor="#f5f5f5"
                arrowColor="#d4d4d4"
                textColor="#000"
            >
                {props.title}
            </ReactTooltip>
        </Fragment>
    );
}
