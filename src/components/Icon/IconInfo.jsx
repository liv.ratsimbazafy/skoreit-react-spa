import React, { Fragment } from "react";
import { AiOutlineExclamationCircle } from "react-icons/ai";
import ReactTooltip from "react-tooltip";
import "./styles.scss";

export default function IconInfo({ ...props }) {
    return (
        <Fragment>
            <span className="info_icon" data-tip data-for={props.title}>
                <AiOutlineExclamationCircle />
            </span>

            <ReactTooltip
                id={props.title}
                place="right"
                effect="solid"
                backgroundColor="#f5f5f5"
                arrowColor="#d4d4d4"
                getContent={function () {
                    return (
                        <div>
                            <h6 className="text-danger font-weight-bold text-uppercase mb-2">
                                attention !
                            </h6>
                            <p className="font-weight-bold">{props.warning}</p>
                        </div>
                    );
                }}
            ></ReactTooltip>
        </Fragment>
    );
}
