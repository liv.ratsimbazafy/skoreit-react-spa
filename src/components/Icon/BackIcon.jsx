import React, { Fragment } from "react";
import { useHistory } from "react-router";
import { MdArrowBack } from "react-icons/md";
import ReactTooltip from "react-tooltip";
import "./styles.scss";

export default function BackIcon({ ...props }) {
    const history = useHistory();

    const redirectTo = (e, history, path) => {
        e.preventDefault();
        history.push(path);
    };
    return (
        <Fragment>
            <span
                onClick={
                    props.asLink
                        ? (e) => redirectTo(e, history, props.path)
                        : (e) => props.onClick(e)
                }                
                className={"back_icon " + props.color}
                data-tip
                data-for={props.title}
                // title={props.title}
            >
                <MdArrowBack />
            </span>

            <ReactTooltip
                id={ props.title}
                place={props.place ?? "bottom"}
                effect="solid"
                backgroundColor="#f5f5f5"
                arrowColor="#d4d4d4"
                textColor="#000"
            >
                {props.title ? props.title : "Retour"}
            </ReactTooltip>
        </Fragment>
    );
}
