import React, { Fragment } from "react";
import { BiEditAlt } from "react-icons/bi";
import { useHistory } from "react-router";
import ReactTooltip from "react-tooltip";
import "./styles.scss";

export default function IconEdit({ ...props }) {
    const history = useHistory();

    const redirectTo = (e, history, path) => {
        e.preventDefault();
        history.push(path);
    };
    return (
        <Fragment>
            <span
                onClick={
                    props.asLink
                        ? (e) => redirectTo(e, history, props.path)
                        : (e) => props.onClick(e)
                }
                className={`edit_icon ${props.customClass}`}
                data-tip
                data-for={props.title}
            >
                <BiEditAlt />
            </span>

            <ReactTooltip
                id={props.title}
                place={props.place ?? "bottom"}
                effect="solid"
                type="dark"
                backgroundColor="#f5f5f5"
                arrowColor="#d4d4d4"
                textColor="#000"
            >
                {props.title}
            </ReactTooltip>
        </Fragment>
    );
}
