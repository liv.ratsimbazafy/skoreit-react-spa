import React, { Fragment } from "react";
import { NavLink } from "react-router-dom";
import { BiMenuAltRight } from "react-icons/bi";
import { openSidebar } from "../store/navigationReducer";
import { useDispatch } from "react-redux";

export default function DisconnectedNav({ toggleSidebar }) {
    const dispatch = useDispatch()
    return (
        <Fragment>
            <div className="nav_center__header">
                <NavLink to="/">                   
                    <h5 className=" nav_center__logo text-primary font-weight-bold text-center text-md-left" style={{fontFamily: "Gtek", letterSpacing: "2px"}}>atlet</h5>
                </NavLink>
                <button className="nav_center__btn">
                    <BiMenuAltRight onClick={() => dispatch(openSidebar())} />
                </button>
            </div>
            <ul className="nav_center__links">
                <li>
                    <NavLink to="/signin">Connexion</NavLink>
                </li>
                <li>
                    <NavLink to="/signup">Ajouter mon équipe</NavLink>
                </li>
            </ul>
        </Fragment>
    );
}
