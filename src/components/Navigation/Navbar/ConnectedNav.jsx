import React, { Fragment } from "react";
import { useHistory } from "react-router";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { BiMenuAltRight } from "react-icons/bi";
import { useDispatch } from "react-redux";
import { set_team_id } from "../../../pages/Team/store/action";
import { openSidebar } from "../store/navigationReducer";

export default function ConnectedNav({ logOut }) {
    const { auth, team } = useSelector((state) => state);
    const history = useHistory();
    const dispatch = useDispatch();

    const handleSelectedTeam = (teamId) => {
        dispatch(set_team_id(teamId));
        history.push(`/home`);
    };

    return (
        <Fragment>
            <div className="nav_center__header">
                <NavLink to="/home">
                    <h5
                        className=" nav_center__logo text-primary font-weight-bold text-center text-md-left"
                        style={{ fontFamily: "Gtek", letterSpacing: "2px" }}
                    >
                        atlet
                    </h5>
                </NavLink>
                <span className="nav_center__btn">
                    <BiMenuAltRight onClick={() => dispatch(openSidebar())} />
                </span>
            </div>
            <ul className="nav_center__links">
                <li>
                    <NavLink to="/team/stats">Stats</NavLink>
                </li>
                <li>
                    <NavLink to="/events">Évènements</NavLink>
                </li>
                <li className="nav-item dropdown">
                    <div
                        className="nav-link"
                        style={{ cursor: "pointer" }}
                        id="team_dropdown"
                    >
                        <span
                            className="nav-link dropdown-toggle m-0 text-dark font-weight-bold"
                            data-toggle="dropdown"
                        >
                            Équipes
                        </span>
                        <div
                            className="dropdown-menu shadow-sm"
                            aria-labelledby="team_dropdown"
                            id="team_dropdown"
                        >
                            {team.user_teams &&
                                team.user_teams.map((team) => {
                                    return (
                                        <p
                                            onClick={() =>
                                                handleSelectedTeam(team.id)
                                            }
                                            className="dropdown-item font-weight-bold"
                                            data-toggle="collapse"
                                            data-target=".navbar-collapse"
                                            key={team.id}
                                        >
                                            {team.name}
                                        </p>
                                    );
                                })}
                        </div>
                    </div>
                </li>
                <li>
                    <NavLink to="/admin/players">Joueurs</NavLink>
                </li>
                <li>
                    <NavLink to="/media">Média</NavLink>
                </li>
                <li
                    className="nav-item dropdown ml-3"
                    style={{
                        borderTopLeftRadius: "20px",
                        borderBottomLeftRadius: "20px",
                    }}
                >
                    <div
                        className="nav-link dropdown-toggle d-flex justify-content-between align-items-center py-0 pl-0"
                        id="user_dropdown"
                        role="button"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                    >
                        <img
                            src={auth && auth.user.profile_img}
                            alt=""
                            className="user_profile_img"
                        />
                        <span className="font-weight-bold text-dark ml-2">
                            {auth && auth.user.name}
                        </span>
                    </div>
                    <div
                        className="dropdown-menu shadow-sm"
                        aria-labelledby="user_dropdown"
                    >
                        <NavLink className="dropdown-item" to="/me">
                            Profile
                        </NavLink>
                        <NavLink className="dropdown-item" to="/collect">
                            Paramètres
                        </NavLink>
                        <div className="dropdown-divider"></div>
                        <p
                            className="dropdown-item"
                            data-toggle="collapse"
                            data-target=".navbar-collapse"
                            onClick={() => logOut()}
                        >
                            <span
                                className="align-middle text-dark font-weight-bold"
                                onClick={() => logOut()}
                            >
                                Se déconnecter
                            </span>
                        </p>
                    </div>
                </li>
            </ul>
        </Fragment>
    );
}
