import React from "react";
import { useSelector, useDispatch } from "react-redux";
import SideBar from "../SideBar/SideBar";
import ConnectedNav from "./ConnectedNav";
import DisconnectedNav from "./DisconnectedNav";
import { logout } from "../../../services/auth";
import { sign_out } from "../../../pages/Auth/store/actions";
import { useHistory } from "react-router";
import "./navbar.scss";

export default function Navbar() {
    const { auth, team } = useSelector((state) => state);
    const dispatch = useDispatch();
    const history = useHistory();

    const logOut = () => {
        logout();
        dispatch(sign_out());
        history.push("/");
    };

    return (
        <div id="nav">
            <div className="nav_center">
                {auth.authenticated && team.team_id ? (
                    <ConnectedNav logOut={logOut} />
                ) : (
                    <DisconnectedNav />
                )}
            </div>
            <SideBar logOut={logOut} />
        </div>
    );
}
