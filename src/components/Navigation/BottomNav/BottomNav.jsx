import React from "react";
import { NavLink, useLocation } from "react-router-dom";
import { FaUserFriends } from "react-icons/fa";
import { BiStats, BiDotsVerticalRounded } from "react-icons/bi";
import { AiFillHome, AiFillCalendar } from "react-icons/ai";
import "./styles.scss";
import { useDispatch } from "react-redux";
import { openSidebar } from "../store/navigationReducer";

export default function BottomNav() {
    const location = useLocation();
    const dispatch = useDispatch();

    const bottomNavLinks = [
        {
            path: "/home",
            icon: <AiFillHome className="h4 m-0" />,
            content: "Accueil",
        },
        {
            path: "/events",
            icon: <AiFillCalendar className="h4 m-0" />,
            content: "Évènements",
        },
        {
            path: "/admin/players",
            icon: <FaUserFriends className="h4 m-0" />,
            content: "Joueurs",
        },
        {
            path: "/team/stats",
            icon: <BiStats className="h4 m-0" />,
            content: "Stats",
        },
        {
            path: "#",
            icon: <BiDotsVerticalRounded className="h4 m-0" />,
            content: "Menu",
            sidebar: true,
        },
    ];

    const toggleSidebar = (e) => {
        e.preventDefault();
        dispatch(openSidebar());
    };

    return (
        <div className="bottom-nav">
            <div className="bottom-nav__container">
                {bottomNavLinks.map((item, i) => {
                    return (
                        <NavLink key={i} to={item.path} className="text-center">
                            <span
                                className="d-block"
                                onClick={
                                    item.sidebar
                                        ? (e) => toggleSidebar(e)
                                        : undefined
                                }
                            >
                                {" "}
                                {item.icon}
                            </span>
                            <small
                                className={
                                    location.pathname === item.path
                                        ? "active_link"
                                        : ""
                                }
                            >
                                {item.content}
                            </small>
                        </NavLink>
                    );
                })}
            </div>
        </div>
    );
}
