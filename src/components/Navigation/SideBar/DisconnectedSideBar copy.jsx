import React from "react";
import { NavLink } from "react-router-dom";
import { AiOutlineLogin, AiOutlineUserAdd } from "react-icons/ai";

import "./sidebar.scss";
import { closeSidebar } from "../store/navigationReducer";
import { useDispatch } from "react-redux";

export default function DisconnectedSideBar() {
    const dispatch = useDispatch();
    const sidebarLinks = [
        {
            path: "/signin",
            icon: <AiOutlineLogin className="h4 mr-3 my-0" />,
            content: "Me connecter",
        },
        {
            path: "/signup",
            icon: <AiOutlineUserAdd className="h4 mr-3 my-0" />,
            content: "Créer un compte",
        },
    ];
    return (
        <ul className="sidebar__links">
            {sidebarLinks.map((item, i) => {
                return (
                    <li key={i} className="bg-light rounded-pill px-3">
                        <NavLink
                            to={item.path}
                            onClick={() => dispatch(closeSidebar())}
                            className="d-flex align-items-center"
                        >
                            {item.icon}
                            <span className="h5 m-0">{item.content}</span>
                        </NavLink>
                    </li>
                );
            })}
        </ul>
    );
}
