import React from "react";
import { useSelector } from "react-redux";
import { IoMdArrowBack } from "react-icons/io";
import ConnectedSideBar from "./ConnectedSideBar";
import DisconnectedSideBar from "./DisconnectedSideBar copy";
import "./sidebar.scss";
import { closeSidebar } from "../store/navigationReducer";
import { useDispatch } from "react-redux";

export default function SideBar({ logOut }) {
    const { auth, team, sidebar } = useSelector((state) => state);
    const dispatch = useDispatch();

    return (
        <aside className={sidebar.isOpen ? "sidebar show" : "sidebar"}>
            <span className="sidebar__close-btn">
                <IoMdArrowBack onClick={() => dispatch(closeSidebar())} />
            </span>
            <div className="sidebar__body">
                {auth.authenticated && team.team_id ? (
                    <ConnectedSideBar logOut={logOut} />
                ) : (
                    <DisconnectedSideBar />
                )}
            </div>
        </aside>
    );
}
