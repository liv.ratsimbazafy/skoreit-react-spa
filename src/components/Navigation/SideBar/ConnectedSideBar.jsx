import React from "react";
import { Link, NavLink } from "react-router-dom";
//import { BsFillImageFill } from "react-icons/bs";
import { FaUserFriends } from "react-icons/fa";
import { MdArrowForwardIos, MdKeyboardArrowDown } from "react-icons/md";
import {
    AiFillHome,
    AiFillCalendar,
    AiOutlinePoweroff,
    AiFillSetting,
    AiOutlineTeam
} from "react-icons/ai";
import { useSelector } from "react-redux";
import { closeSidebar } from "../store/navigationReducer";
import { useDispatch } from "react-redux";

import "./sidebar.scss";

export default function ConnectedSideBar({ toggleSideBar, logOut }) {
    const { user } = useSelector((state) => state.auth);
    const dispatch = useDispatch();
    const logout = () => {
        logOut();
        dispatch(closeSidebar());
    };

    const sidebarLinks = [
        {
            path: "/home",
            icon: <AiFillHome className="" />,
            content: "Back Office",
        },
        {
            path: "/events",
            icon: <AiFillCalendar className="" />,
            content: "Mes Évènements",
        },
        {
            path: "/admin/players",
            icon: <FaUserFriends className="" />,
            content: "Les Joueurs",
        },
        // {
        //     path: "/media",
        //     icon: <BsFillImageFill className="" />,
        //     content: "Mes médias",
        // },
        {
            path: "/settings",
            icon: <AiFillSetting className="" />,
            content: "Paramètres",
        },
    ];
    return (
        <>
            <div className="card text-center border-0 shadow-none bg-transparents">
                <div className="card-body pb-0">
                    <div className="mb-3">
                        <img
                            src={user.profile_img}
                            alt={user.name}
                            className="img-fluide rounded-circle"
                            height="100"
                            width="100"
                            style={{ objectFit: "cover" }}
                        />
                    </div>
                    <h3 className="font-weight-bold">
                        {user && user.name.toUpperCase()}
                    </h3>
                    <p className="font-weight-bold m-0">{user.email}</p>
                    <h5 className="font-weight-bold m-0 text-grey">
                        {user.role}
                    </h5>
                    <Link
                        to="/me"
                        onClick={() => dispatch(closeSidebar())}
                        className="btn btn-black btn-sm rounded-pill py-2 px-3 mt-3"
                    >
                        Voir mon profile
                    </Link>
                </div>
            </div>
            <ul className="sidebar__links">
                {sidebarLinks.map((item, i) => {
                    return (
                        <li key={i} className="border-bottom border-light">
                            <NavLink
                                to={item.path}
                                onClick={() => dispatch(closeSidebar())}
                                className="d-flex justify-content-between align-items-center"
                            >
                                <div className="d-flex align-items-center">
                                    <span className="h4 mr-3 text-secondary1">
                                        {item.icon}
                                    </span>
                                    <h6 className="text-grey font-weight-bold">
                                        {item.content}
                                    </h6>
                                </div>
                                <MdArrowForwardIos className="mr-3 text-secondary1" />
                            </NavLink>
                        </li>
                    );
                })}
              
                <div className="accordion" id="accordionExample">
                    <li
                        className="border-bottom border-light d-flex justify-content-between align-items-center"
                        data-toggle="collapse"
                        data-target="#collapseOne"
                    >
                        <div className="d-flex align-items-center">
                            <span className="h4 mr-3 text-secondary1">
                                <AiOutlineTeam className="" />
                            </span>
                            <h6 className="text-grey font-weight-bold">
                                Mes équipes
                            </h6>
                        </div>
                        <MdKeyboardArrowDown className="h4 mr-3 text-secondary1" />
                    </li>

                    <div
                        id="collapseOne"
                        className="collapse"
                        data-parent="#accordionExample"
                    >
                        
                        <div className="accordion d-flex align-items-center px-4" id="accordionExample">                                
                            <h6 className="text-grey font-weight-bold">
                                Sub item
                            </h6>
                        </div>                                                  
                        
                    </div>
                </div>

                <li className="">
                    <div
                        onClick={() => logout()}
                        className="d-flex align-items-center"
                    >
                        <AiOutlinePoweroff className="h4 mr-3 my-0 text-info" />
                        <span className="h6 m-0 text-info font-weight-bold">
                            Me déconnecter
                        </span>
                    </div>
                </li>
            </ul>
        </>
    );
}
