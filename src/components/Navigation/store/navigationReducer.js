const initialState = {
    isOpen: false,
};

export const openSidebar = (message) => ({
    type: "OPEN_SIDEBAR",
    message,
});

export const closeSidebar = () => ({
    type: "CLOSE_SIDEBAR",
});

export default function sidebarReducer(state = initialState, action) {
    switch (action.type) {
        case "OPEN_SIDEBAR": {
            return {
                ...state,
                isOpen: true,
            };
        }

        case "CLOSE_SIDEBAR": {
            return {
                ...state,
                isOpen: false,
            };
        }

        default: {
            return state;
        }
    }
}
