import React, { useState } from "react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import ModalWrapper from "../Modals/ModalWrapper";
import { userAvatarUpdate } from "../../services/auth";
import { update_profile } from "../../pages/Auth/store/actions";
import { closeModal } from "../Modals/modalReducer";
import { useHistory } from "react-router-dom";


export default function ProfileImageModal() {
    const { auth } = useSelector((state) => state);
    const [file, setFile] = useState("");
    const [imagePreviewUrl, setImagePreviewUrl] = useState("");
    const [showModal, setShowModal] = useState(false);
    const [error, setError] = useState("");
    const history = useHistory();
    const dispatch = useDispatch();

    const handleInputFile = (e) => {
        if (error) setError("");
        setFile({ file: e.target.files[0] });
        let file = e.target.files[0];
        var allowedExtensions =
            /(\.doc|\.docx|\.odt|\.pdf|\.tex|\.txt|\.rtf|\.png|\.jpeg|\.jpg)$/i;

        if (!allowedExtensions.exec(file.name)) {
            setError("Fichier non valide !");
            setImagePreviewUrl("");
        }

        if (file.size > 70000) {
            setError("Le fichier dépasse les 7Mb !");
            setImagePreviewUrl("");
        }

        const reader = new FileReader();
        reader.onloadend = () => {
            setFile(file);
            setImagePreviewUrl(reader.result);
        };
        reader.readAsDataURL(file);
    };

    const updateImageProfile = async (e) => {
        e.preventDefault();
        let form = new FormData();
        form.append("avatar", file);
        form.append("user_id", auth.user.id);

        const res = await userAvatarUpdate(form);
        if (res.status === 200) {
            setShowModal(!showModal);
            dispatch(update_profile(res.data.data.user));
            dispatch(closeModal());
            rerenderComponent();            
        }
    };

    function rerenderComponent() {
        setTimeout(() => {
            history.push("/me");
        }, 500);
    }

    const ImagePreview = ({ url }) => {
        return (
            <img
                src={url}
                alt="user profile"
                style={{
                    width: "100px",
                    height: "100px",
                    borderRadius: "50%",
                    objectFit: "cover",
                }}
            />
        );
    };

    const FileFormats = () => {
        return (
            <div className="mb-5">
                <p className="small text-grey_200 font-weight-bold">
                    Formats PNG | JPG | JPEG
                </p>
                <p className="small text-grey_200 font-weight-bold">Max 5Mb</p>
            </div>
        );
    };

    const ErrorCard = ({ error }) => {
        return <span className="font-weight-bold text-danger h5">{error}</span>;
    };

    const InputFile = () => {
        return (
            <div className="input_file-wrapper">
                <span className="">Importer une photo</span>
                <input
                    type="file"
                    name="profile"
                    id="file-upload"
                    onChange={handleInputFile}
                />
            </div>
        );
    };

    const UploadForm = () => {
        return (
            <form onSubmit={(e) => updateImageProfile(e)}>
                {imagePreviewUrl ? (
                    <div className="d-flex justify-content-center align-items-center mt-3">
                        <button
                            type="submit"
                            className="btn btn-primary"
                            disabled={error ? true : false}
                        >
                            Enregistrer
                        </button>
                        <button
                            onClick={() => {
                                setImagePreviewUrl("");
                                setError("");
                            }}
                            className="btn btn-light ml-3"
                        >
                            Retour
                        </button>
                    </div>
                ) : (
                    <InputFile />
                )}
            </form>
        );
    };

    return (
        <ModalWrapper header="Changer ma photo" backdrop={true} centered>
            <div className="" style={{ minHeight: 200 }}>
                <div className="text-center my-sm">
                    {error ? (
                        <ErrorCard error={error} />
                    ) : imagePreviewUrl ? (
                        <ImagePreview url={imagePreviewUrl} />
                    ) : (
                        <FileFormats />
                    )}
                </div>
                <UploadForm />
            </div>
        </ModalWrapper>
    );
}
