import React from "react";

export default function playerCardList({players}) {
    return (
        players && players.map(player => {
            return( <img key={player.id} src={player.profile_img} alt="" height="200px"/>                
            )
           
        })
    );
}
