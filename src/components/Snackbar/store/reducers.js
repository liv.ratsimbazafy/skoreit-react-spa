const initialState = {
    toggleSnackbar: false,
    snackbarMessage: null,
};

export default function snackbarReducer(state = initialState, action) {
    switch (action.type) {
        case "OPEN_SNACKBAR": {
            return {
                ...state,
                toggleSnackbar: true,
                snackbarMessage: action.message,
            };
        }

        case "CLOSE_SNACKBAR": {
            return {
                ...state,
                toggleSnackbar: false,
                snackbarMessage: null,
            };
        }

        default: {
            return state;
        }
    }
}
