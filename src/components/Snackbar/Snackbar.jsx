import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { closeSnackbar } from "./store/actions";
import { FiX } from "react-icons/fi";
import './styles.scss';

export default function Snackbar() {
    const dispatch = useDispatch();    
    const {toggleSnackbar, snackbarMessage} = useSelector((state) => state.snackbar);    
    
    let timer;
    function handleTimeout() {
        timer = setTimeout(() => {
            dispatch(closeSnackbar());
        }, 3000);
    }

    function handleClose() {
        clearTimeout(timer);
        dispatch(closeSnackbar());
    }

    useEffect(() => {
        if (toggleSnackbar)           
            handleTimeout();
        return () => clearTimeout(timer);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [toggleSnackbar, timer]);

    return (
        toggleSnackbar && (
            <div className={`snackbar`} >
                <p className="m-0">{snackbarMessage}</p>
                <span  onClick={handleClose}>
                    <FiX />
                </span>
            </div>
        )
    );
}


