import React from 'react';
import { Field, useField } from 'formik';

export default function SelectInput({ label, ...props }) {  
    const [ meta ] = useField(props);     
    return(        
        <div className="form-group">
            <label>{ label }</label>
            <Field 
            name={props.name}
            as="select" 
            className={ meta.touched && !!meta.error ? 'form-control errors' : `form-control` }                   
            >
                <option defaultValue={props.defaultOption} value="">{props.defaultOption}</option>
                { props.options && props.options.map(value => {
                    return <option key={ value.id }  value={ value.id } >{ value.name }</option>
                })}
            </Field>
            { meta.touched && meta.error ? <small className="text-danger">{ meta.error }</small> : null}
        </div>
    );
   
}