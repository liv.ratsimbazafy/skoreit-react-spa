import React from "react";
import { useField } from "formik";
import "./styles.scss";

export default function TextInput({ label, ...props }) {
    const [field, meta] = useField(props);

    return(
        <div className="form-group">
            {label && <label className="font-weight-bold text-black">{ label }</label>}
            {/**@todo test if no default value work when edit form */}
            {/* <input {...field} {...props}  value={field.value || ''} className={ meta.touched && !!meta.error ? 'form-control errors' : `form-control`  }/> */}
            <input {...field} {...props} className={ meta.touched && !!meta.error ? 'form-control errors' : `form-control`  }/>
            { (meta.touched && meta.error) ? <small className="text-danger font-weight-bold">{ meta.error }</small> : null}
        </div>
        
     ); 
}
