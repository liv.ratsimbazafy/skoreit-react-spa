import React from "react";
import { useField } from "formik";
import "./styles.scss";

export default function TextAreaInput({ label, ...props }) {
    const [field, meta] = useField(props);

    return (
        <div className="form-group">
            <label>{label}</label>
            {/**@todo test if no default value work when edit form */}
            {/* <input {...field} {...props}  value={field.value || ''} className={ meta.touched && !!meta.error ? 'form-control errors' : `form-control`  }/> */}
            <textarea
                {...field}
                {...props}
                className={
                    meta.touched && !!meta.error
                        ? "form-control errors"
                        : `form-control`
                }
            />
            {meta.touched && meta.error ? (
                <small className="text-danger">{meta.error}</small>
            ) : null}
        </div>
    );
}
