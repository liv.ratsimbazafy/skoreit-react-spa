import React from "react";
import { useDispatch } from "react-redux";
import { closeModal } from "./modalReducer";
import Modal from "react-bootstrap/Modal";

export default function ModalWrapper({
    children,
    header,    
    backdrop,
    centered,
    size
}) {
    const dispatch = useDispatch();

    return (
        <Modal
            show={true}
            onHide={() => dispatch(closeModal())}
            backdrop={backdrop ? backdrop : ""}
            centered={centered}  
            size={size ? size : 'md'}          
        >
            <Modal.Header className="border-bottom-0" closeButton>
                {header && (
                    <Modal.Title className="text-primary font-weight-bold">
                        {header}
                    </Modal.Title>
                )}
            </Modal.Header>
            <Modal.Body>{children}</Modal.Body>            
        </Modal>
    );
}
