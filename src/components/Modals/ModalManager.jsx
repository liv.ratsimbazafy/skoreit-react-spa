import React from 'react';
import TestModal from '../../pages/Media/TestModal';
import TeamEditModal from '../Team/TeamEditModal';
import { useSelector } from 'react-redux';
import EvenTaskItemModal from '../Event/EventTaskItemModal';
import TeamContributionModal from '../Team/TeamContributionModal';
import ProfileImageModal from '../Player/ProfileImageModal';

export default function ModalManager() {

    const getModals = {
        TestModal,
        TeamEditModal,
        EvenTaskItemModal,
        TeamContributionModal,
        ProfileImageModal
    };
    const currentModal = useSelector((state) => state.modals);
    
    let renderedModal;
    if (currentModal) {
      const { modalType, modalProps } = currentModal;
      const ModalComponent = getModals[modalType];
      renderedModal = <ModalComponent {...modalProps} />;
    }
  
    return <span>{renderedModal}</span>;
}