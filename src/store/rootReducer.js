import { combineReducers } from "redux";
import { authReducer } from '../pages/Auth/store/reducer';
import { teamReducer } from "../pages/Team/store/reducer";
import { playerReducer } from "../pages/Players/store/reducer";
import snackbarReducer from "../components/Snackbar/store/reducers";
import modalReducer from "../components/Modals/modalReducer";
import sidebarReducer from "../components/Navigation/store/navigationReducer";


const appReducer = combineReducers({
    auth: authReducer,
    team: teamReducer,
    player: playerReducer,
    snackbar: snackbarReducer,
    modals: modalReducer,
    sidebar: sidebarReducer  
});

const rootReducer = (state, action) => {
    if (action.type === "SIGN_OUT") {
        state = undefined;
    }
    return appReducer(state, action);
};

export default rootReducer;
