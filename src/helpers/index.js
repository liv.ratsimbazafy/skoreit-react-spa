import { useRef, useEffect } from "react";

import { store } from "../store/index";

export const formatTime = (time, type) => {
    switch (type) {
        case "short":
            return time.substring(0, 2);

        case "long":
            return time.substring(0, 5);

        default:
            break;
    }
};

export const isAuthenticated = () => {
    if (typeof window == "undefined") return false;
    const state = store.getState();
    if (state.auth.authenticated) return true;
};

export function useIsMounted() {
    const isMounted = useRef(false);

    useEffect(() => {
        isMounted.current = true;
        return () => (isMounted.current = false);
    }, []);

    return isMounted;
}
