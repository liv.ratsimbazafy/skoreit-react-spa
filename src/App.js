import React, { Fragment } from 'react';
import { useLocation } from 'react-router';
import { Route, Switch } from 'react-router-dom';
import Navbar from './components/Navigation/Navbar/Navbar';
import LandingPage from './pages/LandingPage';
import TeamCreate from './pages/Team/TeamCreate';
import TeamSeasonCreate from './pages/Team/TeamSeasonCreate';
import PlayersListAdmin from './pages/Players/PlayersListAdmin';
import PlayersList from './pages/Players/PlayersList';
import PlayerShow from './pages/Players/PlayerShow';
import EventList from './pages/Event/EventList';
import EventCreate from './pages/Event/EventCreate';
import EventShow from './pages/Event/EventShow';
import PlayerEdit from './pages/Players/PlayerEdit';
import EventEdit from './pages/Event/EventEdit';
import MediaList from './pages/Media/MediaList';
import Home from './pages/Home';
import ProtectedRoute from './helpers/ProtectedRoute';
import Register from './pages/Auth/Register';
import EmailVerification from './pages/Auth/EmailVerification';
import EmailChecker from './pages/Auth/EmailChecker';
import EmailAlreadyChecked from './pages/Auth/EmailAlreadyChecked';
import UserEdit from './pages/User/UserEdit';
import NotFound from './pages/NotFound';
import BottomNav from './components/Navigation/BottomNav/BottomNav';
import TeamGlobalStats from './pages/Team/TeamGlobalStats';
import ModalManager from './components/Modals/ModalManager';
import TeamSettings from './pages/Team/TeamSettings';
import UserProfile from './pages/User/UserProfile';
import SignIn from './pages/Auth/SignIn';
import EventAdminShow from './pages/Event/EventAdminShow';


export default function App() {
    const {key} = useLocation();
    const location = useLocation()
    const hideBottomNavFromPath = ['/', '/signin', '/signup']
    // const dispatch = useDispatch()

    // useEffect(() => {
    //     window.addEventListener('beforeunload', alertUser)
    //     window.addEventListener('unload', handleTabClosing)
    //     return () => {
    //         window.removeEventListener('beforeunload', alertUser)
    //         window.removeEventListener('unload', handleTabClosing)
    //     }
    // })
    
    // const handleTabClosing = () => {
    //     //dispatch(sign_out())
    // }
    
    // const alertUser = (event) => {
    //     event.preventDefault()
    //     event.returnValue = ''
    // }

    return(
        <Fragment>   
            <ModalManager />        
            <Navbar />
            <Switch>
                <Route path="/" exact component={ LandingPage }/>
                <ProtectedRoute path="/home" component={ Home }/>                
                <Route path="/signin" component={ SignIn }/>
                <Route path="/signup" component={ Register }/>
                <Route path="/email/verification/sent" component={ EmailVerification }/>
                <Route path="/email/verification/success/:id" component={ EmailChecker } />
                <Route path="/email/already-verified" component={ EmailAlreadyChecked } />
                <Route path="/team/create" component={ TeamCreate }/>          


                <ProtectedRoute path="/admin/players" component={ PlayersListAdmin } key={'ply_' + key}/> 
                <ProtectedRoute path="/players" component={ PlayersList }/>  
                <ProtectedRoute path="/player/show/:player_id" component={ PlayerShow }/>
                <ProtectedRoute path="/player/edit/:player_id" component={ PlayerEdit }/>

                <ProtectedRoute path="/events" component={ EventList }/>
                <ProtectedRoute path="/event/create" component={ EventCreate }/>
                <ProtectedRoute path="/event/show/:event_id" component={ EventShow } key={'evs_' + key}/>
                <ProtectedRoute path="/event/admin/show/:event_id" component={ EventAdminShow } key={'evs_' + key}/>
                <ProtectedRoute path="/event/edit/:event_id" component={ EventEdit }/>

                <ProtectedRoute path="/media" component={ MediaList }/>

                
                <ProtectedRoute exact path="/me" component={UserProfile} key={'me_' + key}/>

                <ProtectedRoute path="/me/edit" component={UserEdit} />

                <ProtectedRoute path="/season/create" component={ TeamSeasonCreate }/>
                <ProtectedRoute path="/team/:teamId/settings" component={TeamSettings} key={'te_' + key}/>
                <ProtectedRoute path="/team/stats" component={TeamGlobalStats} />
                <Route component={ NotFound } />

            </Switch>
            {!hideBottomNavFromPath.includes(location.pathname) && <BottomNav />}    
                
        </Fragment>
    )
}