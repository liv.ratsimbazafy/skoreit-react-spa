import React, { Fragment, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { get_team_infos } from "../services/team";
import { getPlayer } from "../services/player";
import { useDispatch } from "react-redux";
import { set_team_id, set_user_teams } from "./Team/store/action";
import Dashboard from "./Dashboard";
import TeamConfiguration from "./TeamConfiguration";
import { format } from "date-fns";

import { useIsMounted } from "../helpers";
import Preloader from "../components/Loader/Preloader";

export default function Home() {
    const dispatch = useDispatch();

    const { user } = useSelector((state) => state.auth);
    const { team_id } = useSelector((state) => state.team);

    const [teamInfos, setTeamInfos] = useState([]);
    const [events, setEvents] = useState([]);
    const [currentTime, setCurrentTime] = useState(new Date());
    const [userData, setUserData] = useState("");
    const [loading, setLoading] = useState(true);
    const isMounted = useIsMounted();

    useEffect(() => {
        get_team_infos(team_id)
            .then((res) => {
                if (isMounted.current) {
                    setTeamInfos(res.data.data.team);
                    setEvents(res.data.data.events);
                    dispatch(set_team_id(res.data.data.team.id));
                    dispatch(set_user_teams(res.data.data.user_teams));
                }
            })
            .then(() => setLoading(false));
    }, [dispatch, team_id, isMounted]);

    useEffect(() => {
        getPlayer(team_id, user.id)
            .then((res) => {
                if (isMounted.current)
                    setUserData(res.data.data.player_details);
            })
            .then(() => setLoading(false));

        setInterval(() => {
            setCurrentTime(new Date());
        }, 1000);
    }, [team_id, user, isMounted]);

    const RenderMainSection = () => {
        return (
            <section className="main-section">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="mb-5">
                                <h1 className="text-capitalize font-weight-bold text-black">
                                    Hello {user && user.name}
                                </h1>
                                {isMounted.current && (
                                    <div style={{ width: "150px" }}>
                                        <h6 className="text-dark font-weight-bold">
                                            {currentTime &&
                                                format(
                                                    currentTime,
                                                    "dd MMM H:mm:ss"
                                                )}
                                        </h6>
                                    </div>
                                )}
                            </div>
                            {((teamInfos && teamInfos.season_start === null) ||
                                (teamInfos.players &&
                                    teamInfos.players.length < 2)) && (
                                <p>
                                    Avant de commencer, une petite configuration
                                    de votre équipe est nécessaire pour
                                    bénificier pleinement de toutes les
                                    fonctionnalités de l'application.
                                </p>
                            )}
                        </div>
                    </div>
                </div>

                {teamInfos &&
                    teamInfos.season_start &&
                    teamInfos.players.length > 1 && (
                        <Dashboard
                            teamInfos={teamInfos}
                            events={events}
                            userData={userData}
                        />
                    )}
            </section>
        );
    };

    return (
        <Fragment>
            {loading ? <Preloader /> : <RenderMainSection />}
            {((teamInfos && teamInfos.season_start === null) ||
                (teamInfos.players && teamInfos.players.length < 2)) && (
                <TeamConfiguration teamInfos={teamInfos} />
            )}
        </Fragment>
    );
}
