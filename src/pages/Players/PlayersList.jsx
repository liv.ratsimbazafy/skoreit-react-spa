import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { getTeamPlayers } from "../../services/player";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { FiChevronRight, FiChevronLeft } from "react-icons/fi";
import { MdArrowForward } from "react-icons/md";

import Slider from "react-slick";
import { Link } from "react-router-dom";
import GetWindowDimensions from "../../helpers/GetWindowDimensions";
import "../../components/Player/style.scss";

export default function PayersList() {
    const { width } = GetWindowDimensions();
    const { team_id } = useSelector((state) => state.team);
    const [players, setPlayers] = useState("");
    const [next, setNext] = useState(0);
    const [disable, setDisable] = useState("");

    useEffect(() => {
        const page = false;

        getTeamPlayers(team_id, page).then((res) => {
            if (res.status === 200) {                
                setPlayers(res.data);
            }
        });
    }, [team_id]);

    useEffect(() => {
        if (width <= 2560)
            if (next === 0) setDisable("disabled prev");
            else setDisable("");

        if (width <= 600)
            if (next === players.length - 2) setDisable("disabled next");

        if (width > 598 && width <= 1024)
            if (next === players.length - 2) setDisable("disabled next");

        if (width > 1025 && width <= 2560)
            if (next === players.length - 5) setDisable("disabled next");
    }, [next, disable, players, width]);

    const SamplePrevArrow = (props) => {
        const { onClick } = props;
        return (
            <span
                className={`icon_wrapper right ${
                    disable === "disabled next" && disable
                }`}
            >
                <FiChevronRight
                    onClick={onClick}
                    className={"custom_arrow right"}
                />
            </span>
        );
    };

    const SampleNextArrow = (props) => {
        const { onClick } = props;
        return (
            <span
                className={`icon_wrapper left ${
                    disable === "disabled prev" && disable
                }`}
            >
                <FiChevronLeft
                    onClick={onClick}
                    className="custom_arrow left"
                />
            </span>
        );
    };

    const settings = {
        dots: true,
        infinite: false,
        speed: 500,
        slidesToScroll: 1,
        initialSlide: 0,
        //useCSS: false,
        //cssEase: 'easeOut',
        prevArrow: <SampleNextArrow />,
        nextArrow: <SamplePrevArrow />,
        responsive: [
            {
                breakpoint: 2560,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 2,
                    initialSlide: 5,
                    dots: true,
                },
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 2,
                    initialSlide: 2,
                    dots: true,
                },
            },
            {
                breakpoint: 600,
                settings: {
                    dots: false,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    initialSlide: 1,
                    // centerMode: true,
                    // centerPadding: '20%'
                },
            },
        ],
        afterChange: (current) => setNext(current),
    };

    return (
        <section className="main-section">
            <div className="container">
                <div className="row ">
                    <div className="col-md-12">
                        <div className="d-flex  justify-content-between align-items-center">
                            <h2 className="font-weight-bold">Saison 2021</h2>
                        </div>
                    </div>
                </div>

                <div className="row mt-5">
                    <div className="col-md-12">
                        <div
                            className="d-flex justify-content-between align-items-center"
                            style={{ maxWidth: "130px" }}
                        >
                            <p className="font-weight-bold text-dark">
                                Nbr joueurs{" "}
                            </p>
                            <p className="bg-primary_l_600 text-primary px-2 rounded ml-1 font-weight-bold">
                                {players && players.length}
                            </p>
                        </div>
                    </div>
                    <div className="col-md-12">
                        <div style={{ marginTop: "3rem" }}>
                            {players && (
                                <Slider {...settings}>
                                    {players &&
                                        players.map((player) => {
                                            return (
                                                <div
                                                    key={player.id}
                                                    className=""
                                                >
                                                    <div className="p-3 shadow-sm rounded-lg mx-2">
                                                        <div className="py-3">
                                                            <img
                                                                key={player.id}
                                                                src={
                                                                    player.profile_img
                                                                }
                                                                alt=""
                                                                className="card_profile_img"
                                                            />
                                                        </div>
                                                        <div className="text-center pt-0 px-0">
                                                            <p className="text-primary font-weight-bold text-capitalize">
                                                                {player.name}
                                                            </p>
                                                            <Link
                                                                className="mt-3 d-flex justify-content-center align-items-center text-dark"
                                                                to={`/player/show/${player.id}`}
                                                            >
                                                                <small className="text-dark font-weight-bold">
                                                                    voir{" "}
                                                                </small>
                                                                <MdArrowForward />
                                                            </Link>
                                                        </div>
                                                    </div>
                                                </div>
                                            );
                                        })}
                                </Slider>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
