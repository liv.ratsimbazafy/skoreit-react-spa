export const set_player = player => {
    return {
        type: "SET_PLAYER",
        payload: player
    }
}

export const clear_player = player => {
    return {
        type: "CLEAR_PLAYER",
        payload: player
    }
}
