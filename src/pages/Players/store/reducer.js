const initialState = {
    player: null,
};

export const playerReducer = (state = initialState, action) => {
    if (action.type === "SET_PLAYER") {
        return {
            ...state,
            player: action.payload,
        };
    } else if (action.type === "CLEAR_PLAYER") {
        return {
            ...state,
            player: null,
        };
    } else {
        return state;
    }
};
