import React from "react";

export default function PlayerCreateForm({
    element,
    handleInputChange,
    handleRemoveInput,
    i,
    errors,
}) {
    return (
        <div
            key={i}
            className="form-row bg-light mb-3 py-3 px-3 "
            style={{ borderRadius: "20px" }}
        >
            <div className="col-md-6 m-0">
                <label htmlFor="name">Nom du joueur</label>
                <input
                    type="text"
                    name="name"
                    className="form-control sm"
                    id="name"
                    value={element.name || ""}
                    onChange={(e) => handleInputChange(i, e)}
                    style={{ fontSize: "12px" }}
                />
                {errors.includes("players." + i + ".name") ? (
                    <small className="text-danger ml-2 font-weight-bold">
                        {errors}
                    </small>
                ) : (
                    <small>&nbsp;</small>
                )}
            </div>

            <div className="col-md-6 m-0">
                <label htmlFor="email">Email du joueur</label>
                <input
                    type="text"
                    name="email"
                    className="form-control sm"
                    id="email"
                    value={element.email || ""}
                    onChange={(e) => handleInputChange(i, e)}
                    style={{ fontSize: "12px" }}
                />
                {errors.includes("players." + i + ".email") ||
                errors.includes("email" + i) ? (
                    <small className="text-danger ml-2 font-weight-bold">
                        {errors}
                    </small>
                ) : (
                    <small>&nbsp;</small>
                )}
            </div>
            {i !== 0 ? (
                
                    <button
                        onClick={(e) => handleRemoveInput(i, e)}
                        className="btn btn-sm btn-light"
                    >
                        Retirer
                    </button>
              
            ) : (
                ""
            )}
        </div>
    );
}
