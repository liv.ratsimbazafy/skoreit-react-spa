import React, { useEffect, useState } from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import TextInput from "../../components/Forms/TextInput";
import { useSelector } from "react-redux";
import {
    getPlayer,
    editPlayerDetails,
    editPlayerAccount,
} from "../../services/player";
import Snackbar from "../../components/Snackbar/Snackbar";
import { useDispatch } from "react-redux";
import { openSnackbar } from "../../components/Snackbar/store/actions";
import BackIcon from "../../components/Icon/BackIcon";

export default function PlayerEdit({ match }) {
    const dispatch = useDispatch();
    const [player, setPlayer] = useState("");
    const [playerDetails, setPlayerDetails] = useState("");
    const { team_id } = useSelector((state) => state.team);
    const player_id = match.params.player_id;

    useEffect(() => {
        getPlayer(team_id, player_id).then((res) => {            
            setPlayer(res.data.data.player);
            setPlayerDetails(res.data.data.player_details);
        });
    }, [team_id, player_id]);

    const userInitialValues = {
        name: player ? player.name : " ",
        email: player ? player.email : " ",
    };

    const { nick_name, date_of_birth, height, weight, jersey_number } =
        playerDetails;
    const userDetailsInitialValues = {
        nick_name: nick_name !== null ? nick_name : " ",
        date_of_birth: date_of_birth !== null ? date_of_birth : " ",
        height: height !== null ? height : " ",
        weight: weight !== null ? weight : " ",
        jersey_number: jersey_number !== null ? jersey_number : " ",
    };

    const userAccountValidation = Yup.object({
        name: Yup.string().required("Champs obligatoire."),
        email: Yup.string()
            .email("Invalide email.")
            .required("Champs obligatoire."),
    });

    const userDetailsValidation = Yup.object({
        nick_name: Yup.string().required("Champs obligatoire.").nullable(),
        date_of_birth: Yup.date().required("Champs obligatoire.").nullable(),
        height: Yup.number()
            .typeError("Doit être un entier")
            .required("Champs obligatoire.")
            .nullable(),
        weight: Yup.number()
            .typeError("Doit être un entier")
            .required("Champs obligatoire.")
            .nullable(),
        jersey_number: Yup.number()
            .typeError("Doit être un entier")
            .required("Champs obligatoire.")
            .nullable(),
    });

    const updateUserAccount = (player_accounts, setFieldError) => {
        const data = {
            team_id: team_id,
            player_id: player_id,
            ...player_accounts,
        };
        editPlayerAccount(data)
            .then((res) => {
                if (res.status === 200)
                    dispatch(openSnackbar("Opération bien enregistrée"));
            })
            .catch((err) => {
                if (err && err.response.status === 422) {
                    console.log(err.response.data.data);
                    let message = err.response.data.data.message;
                    message.includes("email")
                        ? setFieldError("email", message)
                        : setFieldError("name", message);
                }
            });
    };

    const updateUserDetails = (values) => {
        const data = { team_id: team_id, player_id: player_id, ...values };
        editPlayerDetails(data).then((res) => {
            if (res.status === 200)
                dispatch(openSnackbar("Opération bien enregistrée"));
        });
    };

    const RenderUserAccountForm = () => {
        return (
            <Formik
                enableReinitialize={true}
                initialValues={userInitialValues}
                onSubmit={(values, { setFieldError }) => {
                    updateUserAccount(values, setFieldError);
                }}
                validationSchema={userAccountValidation}
            >
                {(formik) => {
                    const { isValid, dirty } = formik;
                    return (
                        <div className="col-lg-4">
                            <div className="card shadow-sm rounded-lg">
                                <div className="card-header bg-transparent">
                                    <h5 className="font-weight-bold text-primary">
                                        Compte
                                    </h5>
                                </div>
                                <div className="card-body">
                                    <Form>
                                        <TextInput label="Nom" name="name" />
                                        <TextInput
                                            type="email"
                                            name="email"
                                            label="Adresse email"
                                        />
                                        <button
                                            type="submit"
                                            className="btn btn-primary rounded-pill float-right"
                                            disabled={!(isValid && dirty)}
                                        >
                                            Mettre à jour
                                        </button>
                                    </Form>
                                </div>
                            </div>
                        </div>
                    );
                }}
            </Formik>
        );
    };

    const RenderUserDetailsForm = () => {
        return (
            <Formik
                enableReinitialize={true}
                initialValues={userDetailsInitialValues}
                onSubmit={(values) => {
                    updateUserDetails(values);
                }}
                validationSchema={userDetailsValidation}
            >
                {(formik) => {
                    const { isValid, dirty } = formik;
                    return (
                        <div className="col-lg-7 offset-lg-1 mt-5 mt-lg-0">
                            <div className="card shadow-sm rounded-lg">
                                <div className="card-header bg-transparent">
                                    <h5 className="font-weight-bold text-primary">
                                        Informations
                                    </h5>
                                </div>
                                <div className="card-body">
                                    <Form>
                                        <div className="form-row">
                                            <div className="col-md-6">
                                                <TextInput
                                                    label="Surnom"
                                                    name="nick_name"
                                                />
                                            </div>
                                            <div className="col-md-6">
                                                <TextInput
                                                    type="date"
                                                    name="date_of_birth"
                                                    label="Date de naissance"
                                                />
                                            </div>
                                        </div>

                                        <div className="form-row">
                                            <div className="col-md-4">
                                                <TextInput
                                                    label="Taille (cm)"
                                                    name="height"
                                                />
                                            </div>
                                            <div className="col-md-4">
                                                <TextInput
                                                    label="Poids (kg)"
                                                    name="weight"
                                                />
                                            </div>
                                            <div className="col-md-4">
                                                <TextInput
                                                    label="Num Maillot"
                                                    name="jersey_number"
                                                />
                                            </div>
                                        </div>
                                        <button
                                            type="submit"
                                            className="btn btn-primary rounded-pill float-right"
                                            disabled={!(isValid && dirty)}
                                        >
                                            Mettre à jour
                                        </button>
                                    </Form>
                                </div>
                            </div>
                        </div>
                    );
                }}
            </Formik>
        );
    };

    return (
        <section className="main-section bg-gradient-primary">
            <div className="container">
                <div className="row mb-5">
                    <div className="col-lg-12">
                        <div className="d-flex justify-content-start align-items-center">
                            <BackIcon
                                asLink={true}
                                path="/players"
                                title="Listes des joueurs"
                                color="text-white"
                            />
                            <h4 className="font-weight-bold text-white ml-3">
                                Modifier le joueur
                            </h4>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <RenderUserAccountForm />

                    <RenderUserDetailsForm />
                </div>
                <Snackbar />
            </div>
        </section>
    );
}
