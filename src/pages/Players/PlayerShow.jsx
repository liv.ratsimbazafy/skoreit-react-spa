import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { format } from "date-fns";
import { getPlayer } from "../../services/player";
import BackIcon from "../../components/Icon/BackIcon";

export default function PlayerShow({ match }) {
    const [data, setData] = useState("");
    const [user, setUser] = useState("");
    const { team_id } = useSelector((state) => state.team);
    const player_id = match.params.player_id;

    useEffect(() => {
        getPlayer(team_id, player_id).then((res) => {
            setData(res.data.data);
            setUser(res.data.data.player_details);
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [team_id, player_id]);

    const playerDetails = [
        {
            item: "Saison",
            value: user && user.current_season,
        },
        {
            item: "e-license",
            value: "00 21 56 87 23",
        },
        {
            item: "Membre depuis",
            value: user && format(new Date(user.join_on), "dd LLL yyyy"),
        },
        {
            item: "Surnom",
            value: user && user.nick_name,
        },
        {
            item: "Taille",
            value: user && user.height,
        },
        {
            item: "Poids",
            value: user && user.weight,
        },
        {
            item: "Numéro",
            value: user && user.jersey_number,
        },
    ];

    const PlayerDetailsCard = () => {
        return (
            <div className="card h-min ">
                <div className="card-header">
                    <h5 className="font-weight-bold text-primary">
                        Informations
                    </h5>
                </div>
                <div className="card-body">
                    <ul className="list-group list-group-flush">
                        {playerDetails.map((detail, i) => {
                            return (
                                <li
                                    key={i}
                                    className="list-group-item d-flex justify-content-between align-items-center"
                                >
                                    <small className="text-dark font-weight-bold">
                                        {detail.item}
                                    </small>
                                    <small className="text-dark font-weight-bold">
                                        {detail.value}
                                    </small>
                                </li>
                            );
                        })}
                    </ul>
                </div>
            </div>
        );
    };
    return (
        <section className="main-section">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-start align-items-center">
                            <BackIcon
                                asLink={true}
                                path="/players"
                                title="Liste des joueurs"
                            />
                            <h4 className="font-weight-bold ml-3">
                                Détails du joueur
                            </h4>
                        </div>
                    </div>
                </div>

                <div className="row mt-5">
                    <div className="col-md-4">
                        <div className="card text-center border-0 shadow-none">
                            <div className="card-body pt-0">
                                <div className="mb-3">
                                    <img
                                        src={data && data.player.profile_img}
                                        alt={data && data.player.name}
                                        className="img-fluide"
                                        height="150"
                                        width="150"
                                        style={{
                                            objectFit: "cover",
                                            borderRadius: "20px",
                                        }}
                                    />
                                </div>
                                <h3 className="font-weight-bold">
                                    {data && data.player.name.toUpperCase()}
                                </h3>
                                <p className="font-weight-bold m-0">
                                    {data && data.player.email}
                                </p>
                                <h5 className="font-weight-bold m-0 text-grey">
                                    {data && data.player.role}
                                </h5>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-6 offset-0 offset-md-2 mt-3 mt-md-0">
                        <PlayerDetailsCard />
                    </div>
                </div>
            </div>
        </section>
    );
}
