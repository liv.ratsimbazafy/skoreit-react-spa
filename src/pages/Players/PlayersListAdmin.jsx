import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { getTeamPlayers, removePlayer } from "../../services/player";
import { addNewPlayers } from "../../services/player";
import DataTable from "../../components/DataTable/DataTable";
import PlayerCreate from "./PlayerCreate";
import Snackbar from "../../components/Notification/Snackbar";
import IconAdd from "../../components/Icon/IconAdd";
import Pagination from "react-js-pagination";
import { IoIosArrowForward, IoIosArrowBack } from "react-icons/io";

export default function PayersListAdmin({ history }) {
    const { team_id } = useSelector((state) => state.team);

    const initialValues = { name: "", email: "" };
    const [formFields, setFormFields] = useState([initialValues]);
    const [showForm, setShowForm] = useState(false);
    const [errors, setErrors] = useState([]);
    const [showNotification, setShowNotification] = useState(false);
    const [isLoading, setIsLoading] = useState(false);

    const [players, setPlayers] = useState("");
    const titles = ["Nom", "Email", "Status", "Action"];
    const values = ["name", "email", "email_verified_at"];

    useEffect(() => {
        getAllPlayers();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [team_id]);

    const getAllPlayers = (page = 1) => {
        setIsLoading(true);

        getTeamPlayers(team_id, page).then((res) => {
            if (res.status === 200) {
                setPlayers(res.data);
                setIsLoading(false);
            }
        });
    };

    const handleInputChange = (i, e) => {
        let newFormFields = [...formFields];
        newFormFields[i][e.target.name] = e.target.value;
        setFormFields(newFormFields);
    };

    const handleFormSubmit = async (e) => {
        e.preventDefault();
        const data = { team_id: team_id, players: formFields };
        addNewPlayers(data)
            .then((res) => {
                if (res.status === 201) {
                    history.push("/players");
                    setShowForm(false);
                    setShowNotification(true);
                }
            })
            .catch((err) => {
                if (err.response.status === 422)
                    setErrors(err.response.data.data.message);
            });
    };

    let addFormFields = (e) => {
        e.preventDefault();
        setFormFields([...formFields, initialValues]);
    };

    const removeFormFields = (index, e) => {
        e.preventDefault();
        let newFields = [...formFields];
        if (index === 0) return;
        newFields.splice(index, 1);
        setFormFields(newFields);
    };

    const displayAddPlayerForm = () => {
        setShowForm(!showForm);
        setFormFields([initialValues]);
    };

    const removeTeamPlayer = (e, player_id) => {
        e.preventDefault();
        removePlayer({ team_id, player_id }).then((res) => {
            if (res.status === 200) history.push("/players");
        });
    };

    const renderPaginatedPlayers = () => {
        return (
            <div className="d-flex justify-content-between align-items-center mt-3">
                {players && (
                    <div style={{ width: "250px" }}>
                        {players.current_page * players.per_page >
                        players.total ? (
                            <span>{players.total}</span>
                        ) : (
                            players.current_page * players.per_page
                        )}{" "}
                        / {players.total}
                    </div>
                )}
                <div className="pagination_wrapper  ml-3">
                    {players && (
                        <Pagination
                            totalItemsCount={players.total}
                            activePage={players.current_page}
                            itemsCountPerPage={players.per_page}
                            onChange={(page) => getAllPlayers(page)}
                            prevPageText={<IoIosArrowBack />}
                            nextPageText={<IoIosArrowForward />}
                            firstPageText=""
                            lastPageText=""
                        />
                    )}
                </div>
            </div>
        );
    };

    return (
        <section className="main-section">
            <div className="container">
                <div className="row mb-5">
                    <div className="col-md-12">
                        <div className="d-flex  justify-content-between align-items-center">
                            <h2 className="font-weight-bold">Joueurs</h2>
                            {!showForm && (
                                <IconAdd
                                    title="Ajouter un joueur"
                                    asLink={false}
                                    onClick={displayAddPlayerForm}
                                    customClass="mobile"
                                />
                            )}
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div
                        className={
                            showForm
                                ? "col-xl-6 order-1 order-xl-0"
                                : "col-xl-10"
                        }
                    >
                        {players && players.total >= 5 && (
                            <div className="">{renderPaginatedPlayers()}</div>
                        )}
                        <DataTable
                            colTitles={titles}
                            colValues={values}
                            data={players.data}
                            teamId={team_id}
                            url="player"
                            deleteResource={removeTeamPlayer}
                            fixed={{ height: 320 }}
                            isLoading={isLoading}
                        />
                    </div>

                    <div className="col-xl-5 offset-0 offset-xl-1 mt-xl-0 order-0 order-xl-1 mb-5">
                        {showForm ? (
                            <PlayerCreate
                                formFields={formFields}
                                addFormFields={addFormFields}
                                handleInputChange={handleInputChange}
                                handleFormSubmit={handleFormSubmit}
                                removeFormFields={removeFormFields}
                                errors={errors}
                                displayAddPlayerForm={displayAddPlayerForm}
                            />
                        ) : (
                            ""
                        )}
                        {showNotification ? (
                            <Snackbar message="Action réaliser avec succeés" />
                        ) : (
                            ""
                        )}
                    </div>
                </div>
            </div>
        </section>
    );
}
