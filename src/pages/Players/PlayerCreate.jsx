import React from "react";
import PlayerCreateForm from "./PlayerCreateForm";

import IconAdd from "../../components/Icon/IconAdd";

export default function PlayerCreate({ formFields, ...props }) {
    return (
        <div className="card bg-transparent" style={{ borderRadius: "20px" }}>
            <div className="card-header bg-transparent d-flex justify-content-between align-items-center">
                <h5 className="font-weight-bold m-0">Ajouter des joueurs</h5>
                <IconAdd
                    title="Ajouter plusieurs joueurs"
                    asLink={false}
                    onClick={props.addFormFields}
                />
            </div>
            <div className="card-body">
                <form onSubmit={(e) => props.handleFormSubmit(e)}>
                    {formFields.map((element, i) => {
                        return (
                            <PlayerCreateForm
                                key={i}
                                element={element}
                                handleInputChange={props.handleInputChange}
                                handleRemoveInput={props.removeFormFields}
                                i={i}
                                errors={props.errors}
                            />
                        );
                    })}
                    <div className="card-footer border-0 bg-transparent d-flex justify-content-between align-items-center">
                        <button type="submit" className="btn btn-secondary rounded-pill" disabled={formFields[0].name === '' || formFields[0].email === ''}>
                            Ajouter {formFields.length > 1 ? "les" : "le"}{" "}
                            joueur{formFields.length > 1 ? "s" : ""}
                        </button>
                        <button
                            className="btn btn-light rounded-pill"
                            onClick={props.displayAddPlayerForm}
                        >
                            Annuler
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
}
