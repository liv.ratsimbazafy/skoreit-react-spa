import React from 'react';
import ModalWrapper from '../../components/Modals/ModalWrapper';

export default function PlayersCreateModal({children}) {
    return (
        <ModalWrapper>
            {children}
        </ModalWrapper>
    )
}