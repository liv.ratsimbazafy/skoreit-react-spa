import React from "react";
import { useDispatch } from "react-redux";
import Snackbar from "../../components/Snackbar/Snackbar";
import { openSnackbar } from "../../components/Snackbar/store/actions";
import IconAdd from "../../components/Icon/IconAdd";
import BackIcon from "../../components/Icon/BackIcon";
import { openModal } from "../../components/Modals/modalReducer";

export default function MediaList() {
    const dispatch = useDispatch();

    return (
        <section className="main-section">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h3>Media page</h3>
                        <button
                            onClick={() =>
                                dispatch(openSnackbar("I'm a Snackbar!"))
                            }
                            className="btn btn-warning mt-3"
                        >
                            Snackbar
                        </button>
                    </div>
                </div>
                <div className="row mt-5">
                    <div className="col-md-6">
                        <div className="d-flex justify-content-between">
                            <button className="btn btn-primary ripple">
                                primary
                            </button>
                            <button className="btn btn-danger ripple">
                                danger
                            </button>
                            <button className="btn btn-warning ripple">
                                warning
                            </button>
                            <button className="btn btn-info ripple">
                                info
                            </button>
                            <button className="btn btn-secondary ripple">
                                secondary
                            </button>
                            <button className="btn btn-success ripple">
                                success
                            </button>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="d-flex justify-content-between">
                            <button className="btn btn-light ripple">
                                light
                            </button>
                            <button className="btn btn-dark ripple">
                                dark
                            </button>
                            <button className="btn btn-accent1 ripple">
                                accent1
                            </button>
                            <button className="btn btn-accent2 ripple">
                                accent2
                            </button>
                            <button className="btn btn-accent3 ripple">
                                accent3
                            </button>
                            <button className="btn btn-grey1 ripple">
                                grey1
                            </button>
                        </div>
                    </div>
                </div>
                <div className="d-flex justify-content-between mt-5">
                    <IconAdd />
                    <BackIcon />
                </div>
                <Snackbar />
                <div className="mt-5">
                    <button
                        onClick={() =>
                            dispatch(
                                openModal({
                                    modalType: "TestModal",
                                    modalProps: "Ouff",
                                })
                            )
                        }
                        className="btn btn-dark ripple"
                    >
                        Fire Modal dude
                    </button>
                </div>
            </div>
        </section>
    );
}
