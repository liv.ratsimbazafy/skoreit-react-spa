import React from 'react';
import ModalWrapper from '../../components/Modals/ModalWrapper';

export default function TestModal() {
    return(
        <ModalWrapper header="Test Modal">
            <div>It's working dude ;-)</div>
        </ModalWrapper>
    )
}