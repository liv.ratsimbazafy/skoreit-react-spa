import React, { Fragment, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { getPlayer } from "../../services/player";
import { format } from "date-fns";
import { useIsMounted } from "../../helpers";
import Tabs from "react-bootstrap/Tabs";
import Tab from "react-bootstrap/Tab";
import { openModal } from "../../components/Modals/modalReducer";
import Preloader from "../../components/Loader/Preloader";
import { IoCameraOutline } from "react-icons/io5";
import { MdArrowForward } from "react-icons/md";
import noData from "../../assets/no_data.svg";
import emptyCart from "../../assets/empty_cart.svg";
import "./styles.scss";

export default function UserProfile() {
    const dispatch = useDispatch();
    const { auth, team } = useSelector((state) => state);
    const [data, setData] = useState("");
    const isMounted = useIsMounted();
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        getPlayer(team.team_id, auth.user.id).then((res) => {
            if (isMounted.current && res.status === 200) {
                setData(res.data.data);
                setIsLoading(false);
            }
        });
    }, [team.team_id, auth.user.id, isMounted]);

    const ProfileHero = () => {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-12 px-0">
                        <img
                            style={{
                                width: "100%",
                                objectFit: "cover",
                            }}
                            className="img-fluid profile_bcg"
                            src="https://images.unsplash.com/photo-1618005182384-a83a8bd57fbe?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1064&q=80"
                            alt="basket ball "
                        />
                    </div>
                </div>

                <div className="row">
                    <div className="col-12">
                        <div className="card profile-wrapper mx-auto">
                            {data && (
                                <div className="card-body text-center">
                                    <div className="profile-container">
                                        <img
                                            src={data.player.profile_img}
                                            alt={data.player.name}
                                            className="img-fluide rounded-circle"
                                            height="150"
                                            width="150"
                                            style={{
                                                objectFit: "cover",
                                                border: "5px solid #fff",
                                                background: "#fff",
                                            }}
                                        />
                                        <span
                                            onClick={() =>
                                                dispatch(
                                                    openModal({
                                                        modalType:
                                                            "ProfileImageModal",
                                                        modalProps: {
                                                            data: data,
                                                        },
                                                    })
                                                )
                                            }
                                        >
                                            <IoCameraOutline />
                                        </span>
                                    </div>
                                    <div className="mt-3">
                                        <h3 className="font-weight-bold">
                                            {data.player && data.player.name}
                                        </h3>
                                        <p className="font-weight-bold m-0">
                                            {data.player.email}
                                        </p>

                                        <small className="font-weight-bold m-0 text-primary_l_500">
                                            {data.player_details.role.toUpperCase()}
                                        </small>
                                    </div>
                                    <Link
                                        to="/me/edit"
                                        className="btn btn-sm btn-black rounded-pill mt-2"
                                    >
                                        Modifier
                                    </Link>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        );
    };

    const playerStatsData = [
        {
            title: "Séances",
            content: data && data.events.length,
            color: "text-black",
            modal: true,
        },
        {
            title: "Courses",
            content: data && data.player_details.contribution_paid + "€",
            color: "text-black",
            url: "/players",
        },
        {
            title: "À payer",
            content: data && data.player_details.contribution_unpaid + "€",
            color: "text-black",
            url: "/events",
        },
        {
            title: "Avoirs",
            content: "0€",
            color: "text-black",
            url: "/events",
        },
    ];

    const PlayerStats = () => {
        return (
            <div className="container mt-3">
                <div className="row">
                    <div className="col-12">
                        <div
                            className="d-flex justify-content-between align-items-center mx-auto"
                            style={{ maxWidth: "400px" }}
                        >
                            {playerStatsData.map((item, i) => {
                                return (
                                    <div
                                        key={i}
                                        className="text-center"
                                        style={{ minWidth: "75px" }}
                                    >
                                        <p className="font-weight-bold text-grey_200">
                                            {item.title}
                                        </p>
                                        <h2 className={" " + item.color}>
                                            {item.content}
                                        </h2>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                </div>
            </div>
        );
    };

    const PlayerEvents = () => {
        return (
            <div className="mt-5 pb-5">
                <ul className="list-group list-group-flush mt-3">
                    {data &&
                        data.events.map((event) => {
                            return (
                                <li
                                    key={event.id}
                                    className="list-group-item d-flex justify-content-between align-items-center border-bottom"
                                >
                                    <p>
                                        <span
                                            className="badge badge-dark badge-pill p-2 mr-3"
                                            style={{ minWidth: "50px" }}
                                        >
                                            {event.venue &&
                                                format(
                                                    new Date(event.venue),
                                                    "dd MMM"
                                                )}
                                        </span>
                                        <small className="font-weight-bold text-grey_100">
                                            {" "}
                                            {event.name}
                                        </small>
                                    </p>
                                    <Link to={`/event/show/${event.id}`}>
                                        <small className="text-primary font-weight-bold">
                                            Voir <MdArrowForward />
                                        </small>
                                    </Link>
                                </li>
                            );
                        })}
                </ul>
            </div>
        );
    };

    const PlayerShopping = () => {
        return (
            <div className="mt-5 pb-5">
                <ul className="list-group list-group-flush mt-3">
                    {data &&
                        data.shopping.map((task) => {
                            return (
                                <li
                                    key={task.id}
                                    className="list-group-item d-flex justify-content-between align-items-center border-bottom"
                                >
                                    <p className="d-flex justify-content-center align-items-center">
                                        <span
                                            className="badge badge-accent2 text-white badge-pill p-2 mr-3"
                                            style={{ minWidth: "35px" }}
                                        >
                                            {task.invoice_amount}€
                                        </span>
                                        {task.buy_meals ? (
                                            <small className="text-grey font-weight-bold">
                                                Repas
                                            </small>
                                        ) : (
                                            <small className="text-grey font-weight-bold">
                                                Boisson
                                            </small>
                                        )}
                                    </p>
                                    <Link to={`/event/show/${task.event_id}`}>
                                        <span className="badge badge-secondary text-white badge-pill p-2">
                                            Payé le{" "}
                                            {task &&
                                                format(
                                                    new Date(task.updated_at),
                                                    "dd MMM"
                                                )}{" "}
                                        </span>
                                    </Link>
                                </li>
                            );
                        })}
                </ul>
            </div>
        );
    };

    const PlayerTab = () => {
        return (
            <div className="container mt-5">
                <div className="row">
                    <div className="col-12">
                        <div style={{ minHeight: "300px" }}>
                            <Tabs
                                defaultActiveKey="Séances"
                                id="uncontrolled-tab-example"
                                className="mb-3"
                            >
                                <Tab
                                    eventKey="Séances"
                                    title="Séances"
                                    style={{
                                        maxWidth: "700px",
                                        margin: "auto",
                                    }}
                                >
                                    {data && data.events.length ? (
                                        <PlayerEvents />
                                    ) : (
                                        <NoData
                                            title="Aucun évènement !"
                                            image={noData}
                                        />
                                    )}
                                </Tab>
                                <Tab
                                    eventKey="Courses"
                                    title="Courses"
                                    style={{
                                        maxWidth: "700px",
                                        margin: "auto",
                                    }}
                                >
                                    {data && data.shopping.length ? (
                                        <PlayerShopping />
                                    ) : (
                                        <NoData
                                            title="Le panier est vide !"
                                            image={emptyCart}
                                        />
                                    )}
                                </Tab>
                                <Tab
                                    eventKey="Infos"
                                    title="Infos"
                                    style={{
                                        maxWidth: "700px",
                                        margin: "auto",
                                    }}
                                >
                                    <PlayerDetails />
                                </Tab>
                            </Tabs>
                        </div>
                    </div>
                </div>
            </div>
        );
    };

    const NoData = ({ title, image }) => {
        return (
            <div className="card" style={{ minHeight: "350px" }}>
                <div className="card-body d-flex flex-column justify-content-center align-items-center">
                    <img src={image} alt="" width="100" height="100" />
                    <p className="mt-3 text-primary_l_600 font-weight-bold">
                        {title}
                    </p>
                </div>
            </div>
        );
    };

    const { player_details } = data;
    const playerDetailsData = [
        {
            item: "Saison",
            value: player_details && player_details.current_season,
        },
        {
            item: "e-license",
            value: "00 21 56 87 23",
        },
        {
            item: "Membre depuis",
            value:
                player_details &&
                format(new Date(player_details.join_on), "dd LLL yyyy"),
        },
        {
            item: "Surnom",
            value: player_details && player_details.nick_name,
        },
        {
            item: "Taille",
            value: player_details && player_details.height,
        },
        {
            item: "Poids",
            value: player_details && player_details.weight,
        },
        {
            item: "Numéro",
            value: player_details && player_details.jersey_number,
        },
    ];
    const PlayerDetails = () => {
        return (
            <div className="card">
                <div className="card-body">
                    <ul className="list-group list-group-flush">
                        {playerDetailsData.map((detail, i) => {
                            return (
                                <li
                                    key={i}
                                    className="list-group-item d-flex justify-content-between align-items-center"
                                >
                                    <small className="text-dark font-weight-bold">
                                        {detail.item}
                                    </small>
                                    {detail.value !== null ? (
                                        <small className="text-dark font-weight-bold">
                                            {detail.value}
                                        </small>
                                    ) : (
                                        <small className="text-grey_200 font-weight-bold">
                                            nc
                                        </small>
                                    )}
                                </li>
                            );
                        })}
                    </ul>
                </div>
            </div>
        );
    };

    
    return (
        <section className="">
            {isLoading ? (
                <Preloader />
            ) : (
                <Fragment>
                    <ProfileHero />
                    <PlayerStats />
                    <PlayerTab />
                </Fragment>
            )}
        </section>
    );
}
