import React, { useEffect, useState } from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import TextInput from "../../components/Forms/TextInput";
import { useSelector } from "react-redux";
import {
    getPlayer,
    editPlayerDetails,
    editPlayerAccount,
} from "../../services/player";
import { userPasswordUpdate, logout } from "../../services/auth";
import { sign_out, update_profile } from "../Auth/store/actions";
import Snackbar from "../../components/Snackbar/Snackbar";
import { useDispatch } from "react-redux";
import { openSnackbar } from "../../components/Snackbar/store/actions";
import IconInfo from "../../components/Icon/IconInfo";
import BackIcon from "../../components/Icon/BackIcon";
import { useHistory } from "react-router";

export default function UserEdit({ match }) {
    const dispatch = useDispatch();
    const history = useHistory();
    const [player, setPlayer] = useState("");
    const [playerDetails, setPlayerDetails] = useState("");
    const { team, auth } = useSelector((state) => state);

    useEffect(() => {
        getPlayer(team.team_id, auth.user.id).then((res) => {            
            setPlayer(res.data.data.player);
            setPlayerDetails(res.data.data.player_details);
        });
    }, [team.team_id, auth.user.id]);

    const userInitialValues = {
        name: player ? player.name : " ",
        email: player ? player.email : " ",
    };

    const { nick_name, date_of_birth, height, weight, jersey_number } =
        playerDetails;
    const userDetailsInitialValues = {
        nick_name: nick_name !== null ? nick_name : " ",
        date_of_birth: date_of_birth !== null ? date_of_birth : " ",
        height: height !== null ? height : " ",
        weight: weight !== null ? weight : " ",
        jersey_number: jersey_number !== null ? jersey_number : " ",
    };

    const pwdInitialValues = {
        password: "",
        new_password: "",
    };

    const userAccountValidation = Yup.object({
        name: Yup.string().required("Champs obligatoire."),
        email: Yup.string()
            .email("Invalide email.")
            .required("Champs obligatoire."),
    });

    const userPwdValidation = Yup.object({
        password: Yup.string().required("Champs obligatoire."),
        new_password: Yup.string().required("Champs obligatoire."),
    });

    const userDetailsValidation = Yup.object({
        nick_name: Yup.string().required("Champs obligatoire.").nullable(),
        date_of_birth: Yup.date().required("Champs obligatoire.").nullable(),
        height: Yup.number()
            .typeError("Doit être un entier")
            .required("Champs obligatoire.")
            .nullable(),
        weight: Yup.number()
            .typeError("Doit être un entier")
            .required("Champs obligatoire.")
            .nullable(),
        jersey_number: Yup.number()
            .typeError("Doit être un entier")
            .required("Champs obligatoire.")
            .nullable(),
    });

    const updateUserAccount = (player_accounts, setFieldError) => {
        const data = {
            team_id: team.team_id,
            player_id: auth.user.id,
            ...player_accounts,
        };
        editPlayerAccount(data)
            .then((res) => {
                if (res.status === 200) {
                    const newPlayer = res.data.data;
                    setPlayer((prevState) => ({ ...prevState, ...newPlayer }));
                    dispatch(update_profile(newPlayer));
                    dispatch(openSnackbar("Opération bien enregistrée"));
                }
            })
            .catch((err) => {
                if (err && err.response.status === 422) {
                    console.log(err.response.data.data);
                    let message = err.response.data.data.message;
                    message.includes("email")
                        ? setFieldError("email", message)
                        : setFieldError("name", message);
                }
            });
    };

    const updateUserDetails = (values) => {
        const data = {
            team_id: team.team_id,
            player_id: auth.user.id,
            ...values,
        };
        editPlayerDetails(data).then((res) => {
            if (res.status === 200)
                dispatch(openSnackbar("Opération bien enregistrée"));
        });
    };

    const updateUserPassword = (values, setFieldError) => {
        const user_data = { user_id: auth.user.id, ...values };
        userPasswordUpdate(user_data)
            .then((res) => {
                if (res.status === 204)
                    dispatch(openSnackbar("Opération bien enregistrée"));
            })
            .then(() => {
                logout().then(() => {
                    setTimeout(() => {
                        history.push("/");
                        dispatch(sign_out());
                    }, 2000);
                });
            })
            .catch((err) => {
                if (err && err.response.status === 422) {
                    console.log(err.response.data.data);
                    let message = err.response.data.data.message;
                    message.includes("nouveau")
                        ? setFieldError("new_password", message)
                        : setFieldError("password", message);
                }
            });
    };

    const RenderAccountForm = () => {
        return (
            <div>
                <Formik
                    initialValues={userInitialValues}
                    onSubmit={(values, { setFieldError }) => {
                        updateUserAccount(values, setFieldError);
                    }}
                    validationSchema={userAccountValidation}
                >
                    {(formik) => {
                        const { isValid, dirty } = formik;
                        return (
                            <div className="card">
                                <div className="card-header bg-transparent">
                                    <h5 className="font-weight-bold">
                                        Mon compte
                                    </h5>
                                </div>
                                <div className="card-body">
                                    <Form>
                                        <TextInput label="Nom" name="name" />
                                        <TextInput
                                            type="email"
                                            name="email"
                                            label="Adresse email"
                                        />
                                        <button
                                            type="submit"
                                            className="btn btn-primary rounded-pill float-right"
                                            disabled={!(isValid && dirty)}
                                        >
                                            Mettre à jour
                                        </button>
                                    </Form>
                                </div>
                            </div>
                        );
                    }}
                </Formik>
            </div>
        );
    };

    const RenderPasswordForms = () => {
        return (
            <div className="mt-5">
                <Formik
                    enableReinitialize={true}
                    initialValues={pwdInitialValues}
                    onSubmit={(values, { setFieldError }) => {
                        updateUserPassword(values, setFieldError);
                    }}
                    validationSchema={userPwdValidation}
                >
                    {(formik) => {
                        const { isValid, dirty } = formik;
                        return (
                            <div className="card">
                                <div className="card-header bg-transparent d-flex justify-content-between align-items-center">
                                    <h5 className="font-weight-bold">
                                        Changer mot de passe
                                    </h5>
                                    <IconInfo
                                        warning={warning}
                                        title="warning"
                                    />
                                </div>
                                <div className="card-body">
                                    <Form>
                                        <TextInput
                                            type="password"
                                            name="password"
                                            label="Mot de passe actuel."
                                        />
                                        <TextInput
                                            type="password"
                                            name="new_password"
                                            label="Nouveau mot de passe"
                                        />

                                        <button
                                            type="submit"
                                            className="btn btn-primary rounded-pill float-right"
                                            disabled={!(isValid && dirty)}
                                        >
                                            Mettre à jour
                                        </button>
                                    </Form>
                                </div>
                            </div>
                        );
                    }}
                </Formik>
            </div>
        );
    };

    const RenderUserDetailsForm = () => {
        return (
            <Formik
                enableReinitialize={false}
                initialValues={userDetailsInitialValues}
                onSubmit={(values) => {
                    updateUserDetails(values);
                }}
                validationSchema={userDetailsValidation}
            >
                {(formik) => {
                    const { isValid, dirty } = formik;
                    return (
                        <div className="card">
                            <div className="card-header bg-transparent">
                                <h5 className="font-weight-bold">
                                    Mes détails personnels
                                </h5>
                            </div>
                            <div className="card-body">
                                <Form>
                                    <div className="form-row">
                                        <div className="col-md-6">
                                            <TextInput
                                                label="Surnom"
                                                name="nick_name"
                                            />
                                        </div>
                                        <div className="col-md-6">
                                            <TextInput
                                                type="date"
                                                name="date_of_birth"
                                                label="Date de naissance"
                                            />
                                        </div>
                                    </div>

                                    <div className="form-row">
                                        <div className="col-md-4">
                                            <TextInput
                                                label="Taille (cm)"
                                                name="height"
                                            />
                                        </div>
                                        <div className="col-md-4">
                                            <TextInput
                                                label="Poids (kg)"
                                                name="weight"
                                            />
                                        </div>
                                        <div className="col-md-4">
                                            <TextInput
                                                label="Numéro Maillot"
                                                name="jersey_number"
                                            />
                                        </div>
                                    </div>
                                    <button
                                        type="submit"
                                        className="btn btn-secondary rounded-pill float-right"
                                        disabled={!(isValid && dirty)}
                                    >
                                        Mettre à jour
                                    </button>
                                </Form>
                            </div>
                        </div>
                    );
                }}
            </Formik>
        );
    };

    const warning =
        "Après cette action, vous deviez vous re-connecter à l'application !";
    return (
        <section className="main-section">
            <div className="container">
                <div className="row mb-5">
                    <div className="col-md-12">
                        <div className="d-flex justify-content-start align-items-center">
                            <BackIcon
                                asLink={true}
                                path="/me"
                                title="Profile"
                            />
                            <h4 className="font-weight-bold ml-3">
                                Modifier mon profile
                            </h4>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-4">
                        <RenderAccountForm />
                        <RenderPasswordForms />
                    </div>

                    <div className="col-md-7 offset-md-1 mt-5 mt-md-0">
                        <RenderUserDetailsForm />
                    </div>
                </div>
                <Snackbar />
            </div>
        </section>
    );
}
