import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import TextInput from "../../components/Forms/TextInput";
import { startSession, register } from "../../services/auth";
import { verify_email } from "./store/actions";

export default function Register({ history }) {
    const dispatch = useDispatch();
    const { authenticated, user } = useSelector((state) => state.auth);

    useEffect(() => {
        if (
            authenticated &&
            user.email_verified_at !== "" &&
            user.team_id !== ""
        )
            history.push("/home");
    }, [history, authenticated, user]);

    const initialValues = {
        name: "",
        email: "",
        password: "",
    };

    const validationSchema = Yup.object({
        name: Yup.string().required("Vous devez saisir votre nom !"),
        email: Yup.string()
            .email("Saisir un email valide !")
            .required("Vous devez saisir votre adresse email !"),
        password: Yup.string().required("Vous devez saisir un mot de passe !"),
    });

    const signup = (values, setFieldError) => {
        startSession().then(() => {
            register(values)
                .then((res) => {
                    if (res.status === 201)
                        dispatch(verify_email(res.data.data.user));
                    history.push(
                        "/email/verification/sent?registered=true&verified=false"
                    );
                })
                .catch((err) => {
                    if (err.response.status === 422) {
                        let message = err.response.data.data.message;
                        message.includes("email")
                            ? setFieldError("email", message)
                            : setFieldError("password", message);
                    }
                });
        });
    };

    return (
        <section>
            <div className="container vh-100">
                <div className="row justify-content-center align-items-center h-100">
                    <div className="col-md-7 col-lg-5">
                        <div className="">
                            <h2
                                className="font-weight-bold text-primary"
                                style={{
                                    fontFamily: "Gtek",
                                    letterSpacing: "2px",
                                }}
                            >
                                atlet
                            </h2>
                            <h4 className="mt-1 text-primary font-weight-bold">
                                Créer un compte
                            </h4>
                        </div>

                        <div className="mt-5">
                            <Formik
                                initialValues={initialValues}
                                onSubmit={(values, { setFieldError }) => {
                                    signup(values, setFieldError);
                                }}
                                validationSchema={validationSchema}
                            >
                                {(formik) => {
                                    const { isValid, dirty } = formik;
                                    return (
                                        <Form>
                                            <TextInput
                                                label="Nom"
                                                name="name"
                                                placeholder="Kemba walker"
                                            />
                                            <TextInput
                                                label="Adresse email"
                                                name="email"
                                                placeholder="Mon@adresse.email"
                                            />
                                            <TextInput
                                                label="Mot de passe"
                                                name="password"
                                                type="password"
                                                placeholder="Mot de passe"
                                            />
                                            <div className="d-flex justify-content-between align-items-center mt-5">
                                                <button
                                                    type="submit"
                                                    className="btn btn-primary btn-block ripple btn-lg text-white py-3 rounded-pill shadow-none"
                                                    disabled={
                                                        !(isValid && dirty)
                                                    }
                                                >
                                                    Continuer
                                                </button>
                                            </div>

                                            <div className="mt-3 text-center">
                                               
                                                <small>
                                                    {" "}
                                                    <Link
                                                        to="/signin"
                                                        className="font-weight-bold text-primary"
                                                    >
                                                        Déja un compte ? Se connecter
                                                    </Link>
                                                </small>
                                            </div>
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
