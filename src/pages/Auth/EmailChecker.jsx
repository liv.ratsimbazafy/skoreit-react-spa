import React, { useEffect } from "react";
import { logout, me } from "../../services/auth";
import { sign_in, sign_out } from "./store/actions";
import { set_team_id } from "../Team/store/action";
import { useHistory } from "react-router";
import { useDispatch } from "react-redux";

export default function EmailChecker({ match }) {
    const history = useHistory();
    const dispatch = useDispatch();

    useEffect(() => {
        const user_id = match.params.id;

        me().then((res) => {
            if (
                res.status === 200 &&
                res.data.id == user_id.trim() &&
                res.data.team_id !== ""
            ) {
                dispatch(sign_in(res.data));
                dispatch(set_team_id(res.data.team_id));
                history.push("/home");
            } else if (
                res.status === 200 &&
                res.data.id == user_id.trim() &&
                res.data.team_id === ""
            ) {
                dispatch(sign_in(res.data));
                history.push("/team/create");
            } else {
                logout().then(() => {
                    sign_out();
                    history.push("/");
                });
            }
        });
        /*eslint eqeqeq: "off"*/
    }, [match, dispatch, history]);

    return (
        <section className="main-section">
            <div className="container">
                <div className="row">
                    <div className="col-12">
                        <p>Validation en cours ...</p>
                    </div>
                </div>
            </div>
        </section>
    );
}
