export const sign_in = (user) => {
    return {
        type: "SIGN_IN",
        payload: user,
    };
};

export const sign_out = () => {
    return {
        type: "SIGN_OUT",
    };
};

export const verify_email = (user) => {
    return {
        type: "VERIFY_EMAIL",
        payload: user,
    };
};

export const update_profile = (payload) => {
    return {
        type: "UPDATE_PROFILE",
        payload,
    };
};
