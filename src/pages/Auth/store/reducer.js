const initialState = {
    user: null,
    authenticated: false,
    email_verified: null,
    updated: false,
};

export const authReducer = (state = initialState, action) => {
    if (action.type === "SIGN_IN") {
        return {
            user: action.payload,
            authenticated: true,
            email_verified: true,
            updated: false,
        };
    } else if (action.type === "SIGN_OUT") {
        return {
            ...state,
            user: null,
            authenticated: false,
            email_verified: true,
            updated: false,
        };
    } else if (action.type === "VERIFY_EMAIL") {
        return {
            ...state,
            user: action.payload,
            authenticated: false,
            email_verified: false,
            updated: false,
        };
    } else if (action.type === "UPDATE_PROFILE") {
        return {
            ...state,
            user: {
                ...state.user,
                profile_img: action.payload.profile_img,
                name: action.payload.name,
                email: action.payload.email,
            },
            updated: true,
        };
    } else {
        return state;
    }
};
