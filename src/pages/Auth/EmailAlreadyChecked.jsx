import React from "react";
import { Link } from "react-router-dom";

export default function EmailAlreadyChecked() {
    return (
        <section className="main-section">
            <div className="container vh-100">
                <div className="row justify-content-center align-items-center h-75">
                    <div className="col-12 col-lg-6">
                        <div className="card">
                            <div className="card-body border-0 bg-transparent pt-4 text-center">
                               <h5>Désolé, ce lien est expiré !</h5>
                                <Link to="/" className="btn btn-warning mt-4 mb-3">
                                    Page d'accueil
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
