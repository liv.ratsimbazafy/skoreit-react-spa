import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import TextInput from "../../components/Forms/TextInput";
import { sign_in, verify_email } from "./store/actions";
import { set_team_id } from "../Team/store/action";
import { startSession, authenticate } from "../../services/auth";
import { Link } from "react-router-dom";
import Spinner from "react-bootstrap/Spinner";
import "../../styles/signin.scss";

export default function SignIn({ history }) {
    const dispatch = useDispatch();
    const { authenticated, user } = useSelector((state) => state.auth);

    useEffect(() => {
        if (
            authenticated &&
            user.email_verified_at !== "" &&
            user.team_id !== ""
        )
            history.push("/home");
    }, [history, authenticated, user]);

    const initialValues = {
        email: "",
        password: "",
    };

    const validationSchema = Yup.object({
        email: Yup.string()
            .email("Saisir un email valide !")
            .required("Vous devez saisir votre adresse email !"),
        password: Yup.string().required("Vous devez saisir un mot de passe !"),
    });

    const login = (values, setFieldError) => {
        startSession().then(() => {
            authenticate(values)
                .then((res) => {
                    if (res.status === 200) {
                        if (
                            (res.data.data.user.email_verified_at === null &&
                                res.data.data.user.team_id !== "") ||
                            res.data.data.user.email_verified_at === null
                        ) {
                            dispatch(verify_email(res.data.data.user));
                            history.push(
                                "/email/verification/sent?authenticated=true&verified=false"
                            );
                            return;
                        }
                        if (
                            res.data.data.user.email_verified_at !== null &&
                            res.data.data.user.team_id == ""
                        ) {
                            dispatch(verify_email(res.data.data.user));
                            history.push("/team/create");
                            return;
                        }

                        dispatch(sign_in(res.data.data.user));
                        dispatch(set_team_id(res.data.data.user.team_id));
                        history.push("/home");
                    }
                })
                .catch((err) => {
                    if (err.response.status === 422) {
                        let message = err.response.data.data.message;
                        message.includes("email")
                            ? setFieldError("email", message)
                            : setFieldError("password", message);
                    }
                });
        });
        /*eslint eqeqeq: "off"*/
    };

    const BtnSpinner = () => {
        return (
            <div className="btn btn-black text-white btn-block btn-lg rounded-pill ripple py-3 shadow-none">
                <Spinner animation="border" role="status">
                    <span className="visually-hidden"></span>
                </Spinner>
            </div>
        );
    };

    const RenderForm = () => {
        return (
            <div className="px-3 mt-5">
                <Formik
                    initialValues={initialValues}
                    onSubmit={(values, { setFieldError }) => {
                        login(values, setFieldError);
                    }}
                    validationSchema={validationSchema}
                >
                    {(formik) => {
                        const { isValid, dirty, isSubmitting } = formik;
                        return (
                            <Form>
                                <TextInput
                                    label="Adresse email"
                                    name="email"
                                    placeholder="mon@adresse.email"
                                />
                                <TextInput
                                    label="Mot de passe"
                                    name="password"
                                    type="password"
                                    placeholder="mot de passe"
                                />
                                <small className="float-right mr-2 text-black font-weight-bold">
                                    Mot de passe oublié ?
                                </small>
                                <div
                                    className="d-flex justify-content-between align-items-center"
                                    style={{ marginTop: "5rem" }}
                                >
                                    {isSubmitting ? (
                                        <BtnSpinner />
                                    ) : (
                                        <button
                                            type="submit"
                                            className="btn btn-black text-white btn-block btn-lg rounded-pill ripple py-3 shadow-none"
                                            disabled={!(isValid && dirty)}
                                        >
                                            Continuer
                                        </button>
                                    )}
                                </div>

                                <div className="mt-3 text-center">
                                    <small>
                                        {" "}
                                        <Link
                                            to="/signup"
                                            className="font-weight-bold text-black"
                                        >
                                            Créer mon compte
                                        </Link>
                                    </small>
                                </div>
                            </Form>
                        );
                    }}
                </Formik>
            </div>
        );
    };

    return (
        <section className="signin_container">
            <div className="signin_wrapper">
                <div className="signin_wrapper__header">
                    <div className="pt-5 pl-3">
                        <h2>atlet</h2>
                        <h4 className="mt-1 text-black font-weight-bold">
                            Se connecter
                        </h4>
                    </div>
                </div>
                <div className="signin_wrapper__body">
                    <RenderForm />
                </div>
            </div>
        </section>
    );
}
