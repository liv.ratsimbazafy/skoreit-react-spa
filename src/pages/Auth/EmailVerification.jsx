import React, { useEffect, useState } from "react";
import { AiOutlineCloseCircle } from "react-icons/ai";
import { useSelector } from "react-redux";
import { useLocation } from "react-router";
import {
    resendEmailVerification,
    signUnauthenticatedUser,
} from "../../services/auth";
import queryString from "query-string";

export default function EmailVerification() {
    const { user } = useSelector((state) => state.auth);
    const [message, setMessage] = useState("");
    const [emailMessage, setEmailMessage] = useState("");  
    const location = useLocation();
    const qs = queryString.parse(location.search);
   
    useEffect(() => {
        if (qs.registered === "true" && qs.verified === "false") {
            setEmailMessage("Un email de confirmation a été envoyer à ");
        } else if (qs.authenticated === "false") {            
            setEmailMessage("Ce lien est expiré, un nouveau lien vous a été envoyé !");
            signUnauthenticatedUser({ user_id: qs.user }).then((res) => {
                console.log(res);
                return;
            });
        } else if (qs.authenticated === "true" && qs.verified === "false") {            
            setEmailMessage("Veuillez vérifier votre email, un lien vous a été envoyé !");
            resendEmail();
            return;
        } else {
            return;
        }
    }, [qs.authenticated, qs.registered, qs.user, qs.verified]);

    const resendEmail = () => {
        resendEmailVerification()
            .then((res) => {
                if (res.status === 200) setMessage(res.data);                
            })
            .catch((err) => {
                if (err && err.response.status === 422) setMessage(err.response.data);
            });
    };
    const closePopup = (e) => {
        e.preventDefault();
        setMessage("");
    };

    return (
        <div className="container vh-100">
            <div className="row justify-content-center align-items-center h-75">
                <div className="col-8 col-lg-4">
                    {message && (
                        <div className="py-1 px-3 shadow-sm bg-info rounded mb-5 d-flex justify-content-between align-items-center">
                            {message}
                            <AiOutlineCloseCircle
                                className="h3 m-0 text-light"
                                onClick={(e) => closePopup(e)}
                                title="fermer"
                            />
                        </div>
                    )}
                    <div className="card">
                        <div className="card-header border-0 bg-transparent pt-4">
                            <p>
                                {emailMessage}{" "}
                                <span className="font-weight-bold">
                                    {user && user.email}
                                </span>
                            </p>
                            <button
                                onClick={(e) => resendEmail(e)}
                                className="btn btn-warning my-3 float-right"
                            >
                                Me renvoyer le lien
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
