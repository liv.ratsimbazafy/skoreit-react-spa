export const set_team_id = team_id => {
    return {
        type: "SET_TEAM_ID",
        payload: team_id
    }
}

export const set_user_teams = teams => {
    return {
        type: "SET_USER_TEAMS",
        payload: teams
    }
}
