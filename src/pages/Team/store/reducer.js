const initialState = {
    team_id: null,
    user_teams: null
}

export const teamReducer = (state = initialState , action) => {
    
    if(action.type === "SET_TEAM_ID") {
        return { 
            ...state,                    
            team_id: action.payload
        };        

    } else  if(action.type === "CLEAR_TEAM_ID") {
        return {
            ...state, 
            ...action.payload
        }
    } else  if(action.type === "SET_USER_TEAMS") {
        return {
            ...state, 
            user_teams: action.payload
        }
    } 
    else {
        return state;
    }    
}
