import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import TextInput from "../../components/Forms/TextInput";
import SelectInput from "../../components/Forms/SelectInput";
import { typesAndCategories, teamCreate } from "../../services/team";
import { useHistory } from "react-router";
import { set_team_id } from "./store/action";
import { sign_in } from "../Auth/store/actions";

export default function TeamCreate() {
    const dispatch = useDispatch();
    const history = useHistory();
    const [sports, setSports] = useState([]);
    const [categories, setCategories] = useState([]);

    useEffect(() => {
        getSports();
    }, []);

    async function getSports() {
        const res = await typesAndCategories();
        setSports(res.data.data.sports);
        setCategories(res.data.data.categories);
    }

    const initialValues = {
        team_name: "",
        sport_type_id: "",
        sport_category_id: "",
        season_start: "",
        season_end: ""
    };

    const validationSchema = Yup.object({
        team_name: Yup.string().required("Nom de l'équipe obligatoire."),
        sport_type_id: Yup.string().required("Sport obligatoire."),
        sport_category_id: Yup.string().required("Catégorie obligatoire."),
        season_start: Yup.date().required("Début de saison obligatoire."),
        season_end: Yup.date().required("Fin de saison obligatoire.")  
    });

    const create = (values, setFieldError) => {
        teamCreate(values)
            .then((res) => {
                console.log(res);
                if (res.status === 201) {
                    dispatch(sign_in(res.data.data.user));
                    dispatch(set_team_id(res.data.data.user.team_id));
                    history.push("/home");
                }
            })
            .catch((err) => {
                if (err.response.status === 422) {
                    let message = err.response.data.data.message;
                    message.includes("team_name")
                        ? setFieldError("team_name", message)
                        : setFieldError("sport_category_id", message);
                }
            });
    };

    return (
        <section className="main-section">
            <div className="container vh-100">
                <div className="row justify-content-center align-items-center h-75">
                    <div className="col-md-4">
                        <h1 className="font-weight-bold text-primary ml-1">
                            Ajouter mon équipe
                        </h1>

                        <hr className="mb-5" />
                        <Formik
                            initialValues={initialValues}
                            onSubmit={(values, { setFieldError }) => {
                                create(values, setFieldError);
                                //resetForm();
                            }}
                            validationSchema={validationSchema}
                        >
                            {(isSubmitting, isValid, dirty, meta) => (
                                <Form>
                                    <TextInput
                                        label="Nom de l'équipe"
                                        name="team_name"
                                        placeholder="ex: Chicago bulls "
                                    />
                                    <div className="form-row">
                                        <div className="col-12 col-md-6">
                                            <SelectInput
                                                label="Choisir le sport"
                                                name="sport_type_id"
                                                options={sports}
                                                defaultOption="-- sport --"
                                            />
                                        </div>
                                        <div className="col-12 col-md-6">
                                            <SelectInput
                                                label="Choisir la catégorie"
                                                name="sport_category_id"
                                                options={categories}
                                                defaultOption="-- catégorie --"
                                            />
                                        </div>
                                    </div>
                                    <div className="form-row">
                                        <div className="col-12 col-md-6">
                                            <TextInput
                                                label="Début de saison"
                                                name="season_start"
                                                type="date"
                                            />
                                        </div>
                                        <div className="col-12 col-md-6">
                                            <TextInput
                                                label="Fin de saison"
                                                name="season_end"
                                                type="date"
                                            />
                                        </div>
                                    </div>

                                    <button
                                        type="submit"
                                        className="btn btn-primary rounded-pill btn-lg mt-4"
                                    >
                                        Ajouter mon équipe
                                    </button>
                                </Form>
                            )}
                        </Formik>
                    </div>
                </div>
            </div>
        </section>
    );
}
