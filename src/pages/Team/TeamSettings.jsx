import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { openModal } from "../../components/Modals/modalReducer";
import { get_team_infos } from "../../services/team";
//import { format } from "date-fns";
import Snackbar from "../../components/Snackbar/Snackbar";
import {
    MdOutlineLocationOn,
    MdOutlineSupervisedUserCircle,
    MdOutlinePermContactCalendar,
    MdArrowForward,
    MdSportsBasketball,
    MdOutlineEuro
} from "react-icons/md";
import { Link } from "react-router-dom";

export default function TeamSettings() {
    const { team_id } = useSelector((state) => state.team);
    const [team, setTeam] = useState("");
    const [events, setEvents] = useState("");
    const dispatch = useDispatch();

    useEffect(() => {
        let isMounted = true;
        if (isMounted)
            get_team_infos(team_id).then((res) => {               
                if (res.status === 200) {                   
                    setTeam(res.data.data.team);
                    setEvents(res.data.data.events);
                }
            });
        return (isMounted = false);
    }, [team_id]);

           
    // function formatSeason(start, end) {
    //     return (
    //         start &&
    //         format(new Date(start), "yyyy") +
    //             " - " +
    //             format(new Date(end), "yyyy")
    //     );
    // }

    const teamData = [
        // {   
        //     title: "Saison",
        //     content: formatSeason(team.season_start, team.season_end),
        //     icon: (
        //         <MdSportsBasketball className="h1 text-secondary1 border border-secondary rounded-lg p-2" />
        //     ),
        // },
       
        {
            title: "Coach",
            content: team && team.coach,
            icon: (
                <MdOutlineSupervisedUserCircle className="h2 text-secondary1"/>
            ),
        },
        {
            title: "Cotisation",
            content: team && team.team_details.contribution_price + '€',
            icon: (
                <MdOutlineEuro className="h2 text-secondary1 " />
            ),
            modal: true
        },
        {
            title: "Joueurs",
            content: team && team.players.total,
            icon: (
                <MdOutlinePermContactCalendar className="h2 text-secondary1" />
            ),
            url: "/players",
        },
        {
            title: "Séances",
            content: events && events.total,
            icon: (
                <MdSportsBasketball className="h2 text-secondary1 " />
            ),
            url: "/events",
        },
    ];

    const CardItemContent = ({item}) => {       
        let content;
        if(item.title === 'Coach') {
            content =   <div className="d-flex justify-content-start align-items-center mt-1">
                            <img
                                src={item.content.profile_img}
                                alt="user_profile"
                                style={{
                                    width: "45px",
                                    height: "45px",
                                    objectFit: "cover",
                                    borderRadius: "50%",
                                }}
                            />
                            <p className="ml-2">{item.content.name}</p>
                        </div>
        } else {
            content =   <div className="mt-4 d-flex justify-content-between align-items-center">
                            {item.title === "Saison" ? (
                                <h5 className="">
                                    {item.content}
                                </h5>
                            ) : (
                                <h3 className="font-weight-bold text-secondary1">
                                    {item.content}
                                </h3>
                            )}

                            {item.url && (
                                <Link to={item.url} className="btn btn-link btn-sm text-secondary1">
                                    Détails <MdArrowForward />
                                </Link>
                            )}
                            {item.modal && (
                                <button onClick={() => {dispatch(openModal({modalType: 'TeamContributionModal', modalProps: {team: team}}))}}  className="btn btn-link btn-sm text-secondary1">
                                    Modifier  <MdArrowForward />
                                </button>
                            )}
                        </div>
        }

        return content
    }
    const CardItem = ({ item }) => {        
        return (
            <div className="card shadow-sm" style={{ minHeight: 130 }}>
                <div className="card-body  px-3">
                    <div className="d-flex justify-content-start align-items-center">
                        {item.icon}
                        <p className="font-weight-bold text-grey ml-3">
                            {item.title}
                        </p>
                    </div>
                    <CardItemContent item={item}/>                   
                </div>
            </div>
        );
    };

    const TeamHeader = () => {
        return (
            <div className="row mt-5">
                <div className="col-md-12">
                    <div
                        className="card card-bgi"
                        style={{
                            backgroundImage: `url("https://wallpaperaccess.com/thumb/7175922.jpg")`,
                        }}
                    >
                        <div className="card-bgi-box text-capitalize">
                            <h1 className="text-capitalize ">{team.name}</h1>
                            <h5 className="font-weight-bold">
                                {team.sport} - {team.category}
                            </h5>
                            <div className=" d-flex justify-content-center align-items-center text-white mt-2 ">
                                <MdOutlineLocationOn className="h5 font-weight-bold" />
                                <small className="text-white mb-1 font-weight-bold">
                                    {team.city}
                                </small>
                            </div>
                            <div className="mt-4 text-justify px-5">
                                <small>{team.description}</small>
                            </div>
                            <button
                                onClick={() =>
                                    dispatch(
                                        openModal({
                                            modalType: "TeamEditModal",
                                            modalProps: { team: team },
                                        })
                                    )
                                }
                                className="btn btn-outline-light mt-3 rounded-pill"
                            >
                                Modifier
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    };

    const TeamCards= () => {
        return (
            <div className="row mt-5">
                {teamData.map((item, i) => {
                    return (
                        <div
                            key={i}
                            className="col-6 col-md-3 mb-3 mb-md-0 px-1 px-md-4"
                        >
                            <CardItem item={item} />
                        </div>
                    );
                })}                
            </div>
        );
    };

    return (
        <section className="main-section">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h2 className="font-weight-bold m-0">Paramètres</h2>
                    </div>
                </div>

                <TeamHeader />
                <TeamCards/>
                <Snackbar />
            </div>
        </section>
    );
}
