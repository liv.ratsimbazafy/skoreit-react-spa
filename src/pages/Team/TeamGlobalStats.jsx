import React, { Fragment, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { get_global_stats } from "../../services/team";
import {
    BsCheck2Circle,
    BsCalendarCheck,
    BsCurrencyEuro,
    BsCreditCard2Back,
    BsCartCheck,
} from "react-icons/bs";

import { BiFilterAlt, BiExport } from "react-icons/bi";
import {
    IoIosArrowDown,
    IoIosArrowForward,
    IoIosArrowBack,
} from "react-icons/io";
import { format } from "date-fns";
import Pagination from "react-js-pagination";
import Preloader from "../../components/Loader/Preloader";

export default function TeamGlobalStats() {
    const { team_id } = useSelector((state) => state.team);
    const [stats, setStats] = useState("");
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        let isMounted = true;
        if (isMounted) fetchGlobalStats();
        return (isMounted = false);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    function fetchGlobalStats(page = 1) {
        get_global_stats(team_id, page).then((res) => {
            if (res.status === 200) {setStats(res.data.data); setLoading(false)};
        });
    }

    const renderPlayers = (players) => {
        return players.map((player, i) => {
            return (
                <th
                    key={player.id + "_p"}
                    scope="col"
                    style={{ width: "5%" }}
                    className="text-center"
                >
                    <Link to={`/player/show/${player.id}`}>{player.name}</Link>
                </th>
            );
        });
    };

    const renderEvents = (events) => {
        return events.all_events.data.map((event) => {
            return (
                <tr key={event.id + "_e"}>
                    <td>
                        <h5>
                            <span
                                className="badge badge-light badge-pill py-2"
                                style={{ width: "70px" }}
                            >
                                {format(new Date(event.venue), "dd LLL")}
                            </span>
                        </h5>
                    </td>
                    {events.players.map((player, i) => {
                        return (
                            <td
                                key={player.id + "_ec"}
                                className="text-center align-middle"
                            >
                                {player.events.map(
                                    (userEvent) =>
                                        userEvent.id === event.id && (
                                            <Link
                                                key={userEvent.id}
                                                to={`/event/show/${event.id}`}
                                            >
                                                <BsCheck2Circle className="text-success h4 m-0" />
                                            </Link>
                                        )
                                )}
                            </td>
                        );
                    })}
                </tr>
            );
        });
    };

    const totalFields = [
        {
            totalEvent: "Présences",
            totalDue: "À payer",
            totalPaid: "Courses",
            refund: "Avoir",
        },
    ];
    const renderPlayerTotalEvents = (events) => {
        let user_details = [];
        return (
            events &&
            totalFields.map((field) => {
                return (
                    <tr key={field.id + "_te"} className="">
                        <td>
                            <div className="d-flex align-items-center font-weight-bold text-dark text-capitalize">
                                <span className="mr-2 h5">
                                    <BsCalendarCheck />
                                </span>
                                <span>{field.totalEvent}</span>
                            </div>
                        </td>
                        {events.players.map((player) => {
                            [user_details] = player.user_details;
                            return (
                                <td
                                    key={player.id + "_tec"}
                                    className="text-center"
                                >
                                    {user_details.total_attendances > 0 ? (
                                        <span className="font-weight-bold">
                                            {user_details.total_attendances}
                                        </span>
                                    ) : (
                                        <span>-</span>
                                    )}
                                </td>
                            );
                        })}
                    </tr>
                );
            })
        );
    };

    const renderPlayerTotalDue = (events) => {
        let user_details = [];
        return (
            events &&
            totalFields.map((field) => {
                return (
                    <tr key={field.id + "_td"} className="">
                        <td>
                            <div className="d-flex align-items-center font-weight-bold text-dark text-capitalize">
                                <span className="mr-2 h5">
                                    <BsCurrencyEuro />
                                </span>
                                <span className="">{field.totalDue}</span>
                            </div>
                        </td>
                        {events.players.map((player, i) => {
                            [user_details] = player.user_details;
                            return (
                                <td
                                    key={player.id + "_tdc"}
                                    className="text-center align-middle"
                                >
                                    {player.amount_to_paid !== 0 ? (
                                        <span
                                            className={
                                                player.total_events ===
                                                    events.all_events.length &&
                                                player.total_paid === 0
                                                    ? "font-weight-bold text-danger"
                                                    : "font-weight-bold"
                                            }
                                        >
                                            {user_details.contribution_unpaid}€
                                        </span>
                                    ) : null}
                                </td>
                            );
                        })}
                    </tr>
                );
            })
        );
    };

    const renderPlayerTotalPaid = (events) => {
        let user_details = [];
        return (
            events &&
            totalFields.map((field) => {
                return (
                    <tr key={field.id + "_tp"} className="">
                        <td>
                            <div className="d-flex align-items-center font-weight-bold text-dark text-capitalize">
                                <span className="mr-2 h5">
                                    <BsCartCheck />
                                </span>
                                <span>{field.totalPaid}</span>
                            </div>
                        </td>
                        {events.players.map((player, i) => {
                            [user_details] = player.user_details;
                            return (
                                <td
                                    key={player.id + "_tpc"}
                                    className="text-center align-middle"
                                >
                                    {user_details.contribution_paid >= 0 && (
                                        <div className="d-flex justify-content-center align-items-center">
                                            <p className="font-weight-bold">
                                                {user_details.contribution_paid}
                                                €
                                            </p>
                                            {/* <small className="badge badge-warning ml-1">
                                                12 Nov
                                            </small> */}
                                        </div>
                                    )}
                                </td>
                            );
                        })}
                    </tr>
                );
            })
        );
    };

    const renderPlayerRefund = (events) => {
        let user_details = [];
        return (
            events &&
            totalFields.map((field) => {
                return (
                    <tr key={field.id + "_tr"} className="">
                        <td>
                            <div className="d-flex align-items-center font-weight-bold text-dark text-capitalize">
                                <span className="mr-2 h5">
                                    <BsCreditCard2Back />
                                </span>
                                <span>{field.refund}</span>
                            </div>
                        </td>
                        {events.players.map((player, i) => {
                            [user_details] = player.user_details;
                            return (
                                <td
                                    key={player.id + "_trc"}
                                    className="text-center"
                                >
                                    <span className="font-weight-bold">
                                        {user_details.refund}€
                                    </span>
                                </td>
                            );
                        })}
                    </tr>
                );
            })
        );
    };

    const MainSection = () => {
        return (
            <section className="main-section">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <h2 className="mb-5 font-weight-bold">Statistiques</h2>
                        </div>
                    </div>
                </div>
    
                <div className="container mb-2">
                    <div className="pt-2 pb-4 px-3">
                        <div className="d-flex justify-content-between align-items-center  ">
                            {stats && (
                                <div>
                                    {stats.all_events.current_page *
                                        stats.all_events.per_page >
                                    stats.all_events.total
                                        ? stats.all_events.total
                                        : stats.all_events.current_page *
                                          stats.all_events.per_page}{" "}
                                    / {stats.all_events.total}{" "}
                                    <span>ÉVÈNEMENTS</span>
                                </div>
                            )}
                            <div className="d-flex justify-content-between align-items-center  ">
                                <div
                                    role="button"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                >
                                    <button className="btn btn-light rounded-pill d-none d-md-block">
                                        <span className="mr-2 h5">
                                            <BiFilterAlt />
                                        </span>
                                        Filtres
                                        <span className="ml-2 h5">
                                            <IoIosArrowDown />
                                        </span>
                                    </button>
                                </div>
                                <div
                                    className="dropdown-menu filters"
                                    aria-labelledby="filter_dropdown"
                                >
                                    <p className="dropdown-item">Par évènements</p>
                                    <p className="dropdown-item">Par joueurs</p>
                                </div>
    
                                <div className="ml-3 d-none d-md-block">
                                    <button className="btn btn-light rounded-pill">
                                        <span className="mr-2 h5">
                                            <BiExport />
                                        </span>
                                        Exporter
                                    </button>
                                </div>
                                <div className="pagination_wrapper  ml-3">
                                    {stats && stats.all_events.total >= 5 && (
                                        <Pagination
                                            totalItemsCount={stats.all_events.total}
                                            activePage={
                                                stats.all_events.current_page
                                            }
                                            itemsCountPerPage={
                                                stats.all_events.per_page
                                            }
                                            onChange={(page) =>
                                                fetchGlobalStats(page)
                                            }
                                            prevPageText={<IoIosArrowBack />}
                                            nextPageText={<IoIosArrowForward />}
                                            firstPageText=""
                                            lastPageText=""
                                        />
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    
                <div className="container">
                    <div className="table-responsive">
                        <div className="table-wrapper">
                            <table className="table stats">
                                <thead>
                                    <tr>
                                        <th
                                            scope="col"
                                            className="first-col"
                                            style={{ width: "5%" }}
                                        >
                                            ÉVÈNEMENTS
                                        </th>
                                        {stats
                                            ? renderPlayers(stats.players)
                                            : null}
                                    </tr>
                                </thead>
                                <tbody>
                                    {stats && stats.all_events.data.length > 0 ? (
                                        renderEvents(stats)
                                    ) : (
                                        <tr>
                                            <td>Pas de données</td>
                                        </tr>
                                    )}
    
                                    <tr>
                                        <td></td>
                                    </tr>
    
                                    <tr>
                                        <th>TOTAUX</th>
                                    </tr>
                                    {stats && stats.all_events.data.length > 0 ? (
                                        <Fragment>
                                            {renderPlayerTotalEvents(stats)}
                                            {renderPlayerTotalDue(stats)}
                                            {renderPlayerTotalPaid(stats)}
                                            {renderPlayerRefund(stats)}
                                        </Fragment>
                                    ) : (
                                        <tr>
                                            <td>Pas de données</td>
                                        </tr>
                                    )}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        );
    }

    return (
        <Fragment>
            {loading ? <Preloader /> : <MainSection />}
        </Fragment>
    );
}
