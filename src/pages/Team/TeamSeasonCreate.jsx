import React, { useEffect } from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import TextInput from "../../components/Forms/TextInput";
import { useHistory } from "react-router";
import { team_create_season } from "../../services/team";
import { useSelector } from "react-redux";

export default function TeamSeasonCreate() {    
    const history = useHistory();
    const {team_id} = useSelector(state => state.team)
    //const [errors, setErrors] = useState([]);
    // const [categories, setCategories] = useState([]);

    useEffect(() => {
        
    }, []);
    
    const initialValues = {
        season_start: "",
        season_end: ""
    };

    const validationSchema = Yup.object({
        season_start: Yup.date().required("Date de début de saison obligatoire."),
        season_end: Yup.date().required("Date de fin de saison obligatoire.")        
    });

    const createSeason = (values) => {
        console.log(values);
        team_create_season(values, team_id)
        .then(res => {
            if(res.status === 204)
                history.push('/home');
        })
        .catch(err => {
            //@tdo check if it's correctly working
            // if(err && err.response.status === 422)
            //     setErrors(err.response.data.message);
        })
        // get team_id
        //update team with values
        //if no errors then redirect to /home
    };

    return (
        <section className="main-section">
            <div className="container">
                <div className="row">
                    <div className="col-md-6">
                        <h3 className="mb-5 font-weight-bold">
                            Créer une nouvelle saison
                        </h3>
                        <Formik
                            initialValues={initialValues}
                            onSubmit={(values) => {
                                createSeason(values);
                                
                            }}
                            validationSchema={validationSchema}
                        >
                            {(isSubmitting, isValid, dirty, meta) => (
                                <Form>
                                    <div className="form-row">
                                        <div className="col-12 col-md-6">
                                            <TextInput
                                                label="Date de début de saison"
                                                name="season_start"
                                                type="date"
                                            />
                                        </div>
                                        <div className="col-12 col-md-6">
                                            <TextInput
                                                label="Date de fin de saison"
                                                name="season_end"
                                                type="date"
                                            />
                                        </div>
                                    </div>
                                
                                    <button type="submit" className="btn btn-secondary">
                                        Créer ma saison
                                    </button>
                                </Form>
                            )}
                        </Formik>
                    </div>
                </div>
            </div>
        </section>
    );
}
