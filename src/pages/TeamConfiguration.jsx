import React from "react";
import { Link } from "react-router-dom";
import { AiOutlineCheckCircle } from "react-icons/ai";

export default function TeamConfiguration({ teamInfos }) {
    const linkStyle = {
        color: 'light',
        cursor: 'not-allowed',
        opacity: '0.5',
        textDecoration: 'none'
    }
    return (
        <section className="main-section">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h4 className="text-uppercase font-weight-bold">
                            Configurer mon équipe
                        </h4>                       
                    </div>
                </div>

                <div className="row mt-3">
                    <div className="col-md-12">
                        <div
                            className={
                                teamInfos.season_start === null
                                    ? "bg-warning"
                                    : "bg-light"
                            }
                        >
                            <div className="card-body d-flex justify-content-between align_items-center">
                                <Link to="/season/create" style={teamInfos.season_start !== null ? linkStyle : {}}>
                                    <h5 className="m-0">
                                        1 - Créer une nouvelle saison
                                    </h5>
                                </Link>
                                {teamInfos.season_start !== null && (
                                    <AiOutlineCheckCircle className="align-middle h3 m-0 text-success" />
                                )}
                            </div>
                        </div>
                    </div>
                </div>

               
                    <div className="row mt-3">
                        <div className="col-md-12">
                            <div className={
                                teamInfos.players.length < 2
                                    ? "bg-warning"
                                    : "bg-light"
                            } >
                                <div className="card-body  d-flex justify-content-between align_items-center">
                                    <Link to={teamInfos.players.length >= 2 ? '#' : '/players'} style={teamInfos.players.length >= 2 ? linkStyle : {}}>
                                        <h5>
                                            2 - Ajouter des membres à mon
                                            équipes
                                        </h5>
                                    </Link>
                                    {teamInfos.players.length >= 2 && (
                                    <AiOutlineCheckCircle className="align-middle h3 m-0 text-success" />
                                )}
                                </div>
                            </div>
                        </div>
                    </div>
                
            </div>
        </section>
    );
}
