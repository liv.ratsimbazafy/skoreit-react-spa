import React, { Fragment, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { getTeamEvents, deleteEvent } from "../../services/event";
import IconAdd from "../../components/Icon/IconAdd";
import DataTable from "../../components/DataTable/DataTable";
import { IoIosArrowForward, IoIosArrowBack } from "react-icons/io";
import Pagination from "react-js-pagination";
import noData from "../../assets/no_data.svg";
import Preloader from "../../components/Loader/Preloader";

export default function EventList() {
    const [render, setRender] = useState(true);
    const [isLoading, setIsLoading] = useState(false);
    const [allEvents, setAllEvents] = useState([]);
    const { team_id } = useSelector((state) => state.team);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        let isMounted = true;
        if (render || isMounted) getEvents();
        return (isMounted = false);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [render]);

    const getEvents = (page = 1) => {
        setIsLoading(true);
        getTeamEvents(team_id, page).then((res) => {
            setRender(false);
            setAllEvents(res.data.data.all_events);
            setIsLoading(false);
            setLoading(false);
        });
    };

    const eventDelete = (e, event_id) => {
        e.preventDefault();
        deleteEvent(team_id, event_id).then((res) => {
            if (res.status === 204) setRender(true);
        });
    };

    const titles = ["Date", "Titre", "Action"];
    const values = ["venue", "name"];

    const PaginationLinks = ({ data }) => {
        return (
            <div className="d-flex justify-content-between align-items-center mt-3 text-grey font-weight-bold">
                {data && (
                    <div style={{ width: "100px" }}>
                        {data.current_page * data.per_page > data.total ? (
                            <span>{data.total}</span>
                        ) : (
                            data.current_page * data.per_page
                        )}{" "}
                        sur {data.total}
                    </div>
                )}
                <div className="pagination_wrapper  ml-3">
                    {data && (
                        <Pagination
                            totalItemsCount={data.total}
                            activePage={data.current_page}
                            itemsCountPerPage={data.per_page}
                            onChange={(page) => getEvents(page)}
                            prevPageText={<IoIosArrowBack />}
                            nextPageText={<IoIosArrowForward />}
                            firstPageText=""
                            lastPageText=""
                        />
                    )}
                </div>
            </div>
        );
    };

    const NoEvent = ({ title }) => {
        return (
            <div className="card" style={{ minHeight: "350px" }}>
                <div className="card-body d-flex flex-column justify-content-center align-items-center">
                    <img src={noData} alt="" width="100" height="100" />
                    <p className="mt-3 text-primary_l_600 font-weight-bold">
                        {title}
                    </p>
                    <div className="text-center mt-3">
                        <Link
                            to="/event/create"
                            className="btn btn-primary btn-sm"
                        >
                            Ajouter un évènement
                        </Link>
                    </div>
                </div>
            </div>
        );
    };

    const MainSection = () => {
        return (
            <section className="main-section">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="d-flex justify-content-between align-items-center">
                                <h2 className="font-weight-bold">Évènements</h2>
                                <IconAdd
                                    path="/event/create"
                                    title="ajouter un évènement"
                                    asLink={true}
                                    customClass="mobile"
                                />
                            </div>
                        </div>
                    </div>

                    <div className="row mt-5">
                        <div className="col-md-12">
                            {allEvents && allEvents.total >= 5 && (
                                <PaginationLinks data={allEvents} />
                            )}

                            {allEvents.data && allEvents.data.length !== 0 && (
                                <DataTable
                                    colTitles={titles}
                                    data={allEvents.data}
                                    colValues={values}
                                    isDate={true}
                                    url="event"
                                    deleteResource={eventDelete}
                                    fixed={{ height: 320 }}
                                    isLoading={isLoading}
                                />
                            )}

                            {allEvents.data && allEvents.data.length === 0 && (
                                <NoEvent title="Aucun évènement !" />
                            )}
                        </div>
                    </div>
                </div>
            </section>
        );
    };

    return <Fragment>{loading ? <Preloader /> : <MainSection />}</Fragment>;
}
