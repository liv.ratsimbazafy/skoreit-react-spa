import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import { getEventById, editEvent } from "../../services/event";
import TextInput from "../../components/Forms/TextInput";
import { useDispatch } from "react-redux";
import { openSnackbar } from "../../components/Snackbar/store/actions";
import Snackbar from "../../components/Snackbar/Snackbar";
import BackIcon from "../../components/Icon/BackIcon";

export default function EventEdit({ match }) {
    const dispatch = useDispatch();
    const { team_id } = useSelector((state) => state.team);
    const [event, setEvent] = useState({
        name: "",
        description: "",
        venue: "",
        time: "",
        address: "",
    });

    const event_id = match.params.event_id;

    useEffect(() => {
        getEventById(event_id).then((res) => {
            setEvent({
                name: res.data.data.event.name,
                description: res.data.data.event.description,
                venue: res.data.data.event.venue,
                time: res.data.data.event.time,
                address: res.data.data.event.address,
            });
        });
    }, [team_id, event_id]);

    const eventValidation = Yup.object({
        name: Yup.string().required("Champs obligatoire.").nullable(),
        description: Yup.string().required("Champs obligatoire.").nullable(),
        venue: Yup.date().required("Champs obligatoire.").nullable(),
        time: Yup.string().required("Champs obligatoire.").nullable(),
        address: Yup.string().required("Champs obligatoire.").nullable(),
    });

    const updateEvent = (values) => {
        editEvent(values, event_id).then((res) => {
            if (res.status === 200)
                dispatch(openSnackbar("Opération bien enregistrée"));
        });
    };

    return (
        <section className="main-section bg-gradient-primary">
            <div className="container">
                <div className="row mb-5">
                    <div className="col-md-12">                        
                        <div className="d-flex justify-content-start align-items-center">
                            <BackIcon
                                asLink={true}
                                path="/players"
                                title="Listes des joueurs"
                                color="text-white"
                            />
                            <h4 className="font-weight-bold ml-3 text-white">
                                Modifier l'évènement
                            </h4>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-8">
                        <Formik
                            enableReinitialize={true}
                            initialValues={event}
                            onSubmit={(values) => {
                                updateEvent(values);
                            }}
                            validationSchema={eventValidation}
                        >
                            {formik => {
                                const {isValid, dirty} = formik;
                                return(
                                    <div className="card rounded-lg">
                                        <div className="card-body">
                                            <Form>
                                                <div className="form-row">
                                                    <div className="col-md-6">
                                                        <TextInput
                                                            label="Titre"
                                                            name="name"
                                                        />
                                                    </div>
                                                    <div className="col-md-3">
                                                        <TextInput
                                                            label="Date"
                                                            name="venue"
                                                            type="date"
                                                        />
                                                    </div>
                                                    <div className="col-md-3">
                                                        <TextInput
                                                            label="Heure"
                                                            name="time"
                                                            type="time"
                                                        />
                                                    </div>
                                                </div>

                                                <TextInput
                                                    name="description"
                                                    label="Description"
                                                />

                                                <TextInput
                                                    label="Localisation"
                                                    name="address"
                                                />
                                                <button
                                                    type="submit"
                                                    className="btn btn-primary rounded-pill float-right mt-3"
                                                    disabled={!(isValid && dirty)}
                                                >
                                                    Mettre à jour
                                                </button>
                                            </Form>
                                        </div>
                                    </div>
                                )
                            }}                           
                        </Formik>
                    </div>
                </div>
                <Snackbar />
            </div>
        </section>
    );
}
