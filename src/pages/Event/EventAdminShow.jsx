import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import {
    getEventById,
    adminAddParticipant,
    adminRemoveParticipant,
} from "../../services/event";
import format from "date-fns/format";
import { formatTime } from "../../helpers";
import { BsWatch, BsTrash } from "react-icons/bs";
import { IoPersonAddOutline } from "react-icons/io5";
import { useHistory } from "react-router-dom";
import noData from "../../assets/no_data.svg";
import Form from "react-bootstrap/esm/Form";

export default function EventAdminShow({ match }) {
    const { team } = useSelector((state) => state);
    const history = useHistory();
    const event_id = match.params.event_id;
    const [data, setData] = useState("");
    const [usersIn, setUsersIn] = useState([]);
    const [usersOut, setUsersOut] = useState([]);

    useEffect(() => {
        let isMounted = true;
        if (isMounted) getEvent(event_id);
        return () => (isMounted = false);
    }, [event_id]);

    const getEvent = (event_id) => {
        getEventById(event_id).then((res) => {
            console.log(res.data.data);
            setData(res.data.data);
        });
    };

    const addPlayer = (player_id) => {
        adminAddParticipant(event_id, team.team_id, player_id).then((res) => {
            history.push("/event/admin/show/" + event_id);
        });
    };

    const removePlayer = (player_id) => {
        adminRemoveParticipant(event_id, team.team_id, player_id).then(
            (res) => {
                history.push("/event/admin/show/" + event_id);
            }
        );
    };

    const EventCard = () => {
        return (
            <div
                className="card card-bgi rounded-lg"
                style={{
                    backgroundImage: `url("https://wallpaperaccess.com/full/820979.jpg")`,
                }}
            >
                <div className="card-bgi-box">
                    <h1 className="text-capitalize font-weight-light">
                        {data && format(new Date(data.event.venue), "MMM")}
                    </h1>
                    <p className="text-capitalize mt-n3 display-3 font-weight-light">
                        {data && format(new Date(data.event.venue), "dd")}
                    </p>
                    <p className=" text-center mt-n2 h5 font-weight-light d-flex justify-content-center align-items-center">
                        <BsWatch className="" />
                        <span className="">
                            {data && formatTime(data.event.time, "long")}
                        </span>
                    </p>
                </div>
            </div>
        );
    };

    const PlayersCardAction = ({ action, player }) => {
        if (action) {
            return (
                <span
                    style={{ cursor: "pointer" }}
                    onClick={(e) => addPlayer(player)}
                    className="bg-primary_l_700 py-1 px-2 rounded-pill"
                >
                    <IoPersonAddOutline className="text-primary h5 mb-0" />
                </span>
            );
        } else {
            return (
                <span
                    style={{ cursor: "pointer" }}
                    onClick={(e) => removePlayer(player)}
                    className="bg-primary_l_700 py-1 px-2 rounded-pill"
                >
                    <BsTrash className="text-info h5 mb-0" />
                </span>
            );
        }
    };

    const handlePlayersInCheckbox = (item) => {
        const isChecked = usersIn.some((user) => user.id === item.id);
        if (isChecked) setUsersIn(usersIn.filter((user) => user.id !== item.id));
        else setUsersIn(usersIn.concat(item));
    };

    const handlePlayersOutCheckbox = (item) => {
        const isChecked = usersOut.some((user) => user.id === item.id);
        if (isChecked) setUsersOut(usersOut.filter((user) => user.id !== item.id));
        else setUsersOut(usersOut.concat(item));
    };


    const PlayersInCard = ({ players, header, action }) => {
        
        return (
            <div className="col-md-6">
                <div className="mt-5 rounded-lg shadow py-4">
                    <div className="d-flex justify-content-between align-items-center px-3 mb-3">
                        <div className="d-flex justify-content-start align-items-center">
                            <p className="mr-2 bg-primary_l_700 text-primary py-2 px-3 rounded-pill font-weight-bold">
                                {players && players.length}
                            </p>
                            <h5 className="text-dark font-weight-bold">
                                {header}
                            </h5>
                        </div>
                        {usersIn.length > 0 && (
                            <button className="btn btn-sm btn-primary rounded-pill">
                                Supprimer
                            </button>
                        )}                       
                    </div>
                    <ul
                        className="list-group list-group-flush"
                        style={{ maxHeight: "350px", overflowY: "scroll" }}
                    >
                        {players &&
                            players.map((player, index) => {
                                return (
                                    <li
                                        key={player.id}
                                        className="list-group-item d-flex justify-content-between align-items-center border-bottom"
                                    >
                                        <div className="d-flex justify-content-between align-items-center">
                                            <div className="d-flex justify-content-between align-items-center form-check">
                                                <input
                                                    key={`cb-${index}`}
                                                    value={player.id}
                                                    type="checkbox"
                                                    checked={usersIn.some(
                                                        (user) =>
                                                            user.id ===
                                                            player.id
                                                    )}
                                                    onChange={() =>
                                                        handlePlayersInCheckbox(player)
                                                    }
                                                />
                                            </div>
                                            <img
                                                src={player.profile_img}
                                                alt="user_profile"
                                                style={{
                                                    width: "45px",
                                                    height: "45px",
                                                    objectFit: "cover",
                                                    borderRadius: "50%",
                                                }}
                                            />
                                            <p className="ml-2">
                                                {player.name}
                                            </p>
                                        </div>
                                        <PlayersCardAction
                                            action={action}
                                            player={player.id}
                                        />
                                    </li>
                                );
                            })}
                    </ul>
                </div>
            </div>
        );
    };

    const PlayersOutCard = ({ players, header, action }) => {        
        return (
            <div className="col-md-6">
                <div className="mt-5 rounded-lg shadow py-4">
                    <div className="d-flex justify-content-between align-items-center px-3 mb-3">
                        <div className="d-flex justify-content-start align-items-center">
                            <p className="mr-2 bg-primary_l_700 text-primary py-2 px-3 rounded-pill font-weight-bold">
                                {players && players.length}
                            </p>
                            <h5 className="text-dark font-weight-bold">
                                {header}
                            </h5>
                        </div>
                        {usersOut.length > 0 && (
                            <button className="btn btn-sm btn-primary rounded-pill">
                                Ajouter
                            </button>
                        ) }
                       
                    </div>
                    <ul
                        className="list-group list-group-flush"
                        style={{ maxHeight: "350px", overflowY: "scroll" }}
                    >
                        {players &&
                            players.map((player, index) => {
                                return (
                                    <li
                                        key={player.id}
                                        className="list-group-item d-flex justify-content-between align-items-center border-bottom"
                                    >
                                        <div className="d-flex justify-content-between align-items-center">
                                            <div className="d-flex justify-content-between align-items-center form-check">
                                                <input
                                                    key={`cb-${index}`}
                                                    value={player.id}
                                                    type="checkbox"
                                                    checked={usersOut.some(
                                                        (user) =>
                                                            user.id ===
                                                            player.id
                                                    )}
                                                    onChange={() =>
                                                        handlePlayersOutCheckbox(player)
                                                    }
                                                />
                                            </div>
                                            <img
                                                src={player.profile_img}
                                                alt="user_profile"
                                                style={{
                                                    width: "45px",
                                                    height: "45px",
                                                    objectFit: "cover",
                                                    borderRadius: "50%",
                                                }}
                                            />
                                            <p className="ml-2">
                                                {player.name}
                                            </p>
                                        </div>
                                        <PlayersCardAction
                                            action={action}
                                            player={player.id}
                                        />
                                    </li>
                                );
                            })}
                    </ul>
                </div>
            </div>
        );
    };

    const NoEvent = ({ title }) => {
        return (
            <div className="col-md-6">
                <div
                    className="card rounded-lg shadow-sm"
                    style={{ minHeight: "350px" }}
                >
                    <div className="card-body d-flex flex-column justify-content-center align-items-center">
                        <img src={noData} alt="" width="100" height="100" />
                        <p className="mt-3 text-primary_l_600 font-weight-bold">
                            {title}
                        </p>
                    </div>
                </div>
            </div>
        );
    };

    return (
        <section className="main-section">
            <div className="container">
                <div className="row">
                    <div className="col-md-6">
                        <EventCard />
                    </div>
                </div>

                <div className="row mt-5">
                    {data && data.players_in.length > 0 && (
                        <PlayersInCard
                            players={data.players_in}
                            header="Joueurs présent"                            
                        />
                    )}
                    {data && data.players_in.length === 0 && (
                        <NoEvent title="Aucun joueur inscrit !" />
                    )}
                    <PlayersOutCard
                        players={data.players_out}
                        header="Joueurs absent"                       
                    />
                </div>
            </div>
        </section>
    );
}
