import React from "react";
import { useHistory } from "react-router";
import { useSelector } from "react-redux";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import { createEvent } from "../../services/event";
import TextInput from "../../components/Forms/TextInput";
import BackIcon from "../../components/Icon/BackIcon";

export default function EventCreate() {
    const { team_id } = useSelector((state) => state.team);
    const history = useHistory();

    const initialValues = {
        name: "",
        description: "",
        venue: "",
        time: "",
        address: "",
    };

    const validationSchema = Yup.object({
        name: Yup.string().required("Champs obligatoire."),
        venue: Yup.date().required("Champs obligatoire."),
        time: Yup.string().required("Champs obligatoire."),
        address: Yup.string().required("Champs obligatoire."),
    });

    const addEvent = (values, setFieldError) => {
        const data = { ...values, team_id };
        createEvent(data)
            .then((res) => {
                if (res.status === 201) {
                    history.push("/events");
                }
            })
            .catch((err) => {
                if (err && err.response.status === 422) {
                    let message = err.response.data.data.message;
                    message.includes("heure")
                        ? setFieldError("time", message)
                        : setFieldError("date", message);
                }
            });
    };

    return (
        <section className="main-section">
            <div className="container">
                <div className="row">
                    <div className="col-md-6">
                        <div className="d-flex justify-content-start align-items-center mb-5">
                            <BackIcon
                                asLink={true}
                                path="/events"
                                title="Les évènements"
                            />
                            <h4 className="font-weight-bold ml-3">
                                Ajouter un évènement
                            </h4>
                        </div>
                        <Formik
                            initialValues={initialValues}
                            onSubmit={(values, { setFieldError }) => {
                                addEvent(values, setFieldError);
                            }}
                            validationSchema={validationSchema}
                        >
                            {(formik) => {
                                const { isValid, dirty } = formik;
                                return (
                                    <Form>
                                        <div className="form-row">
                                            <div className="col-md-6">
                                                <TextInput
                                                    label="Titre"
                                                    name="name"
                                                    placeholder="ex: Entrainement"
                                                />
                                            </div>
                                            <div className="col-md-3">
                                                <TextInput
                                                    label="Date"
                                                    name="venue"
                                                    type="date"
                                                />
                                            </div>
                                            <div className="col-md-3">
                                                <TextInput
                                                    label="Heure"
                                                    name="time"
                                                    type="time"
                                                />
                                            </div>
                                        </div>

                                        <TextInput
                                            label="Description"
                                            name="description"
                                            placeholder="ex: Entrainement de la semaine"
                                        />
                                        <TextInput
                                            label="Lieu"
                                            name="address"
                                            placeholder="ex: 13, rue du drible 75000"
                                        />
                                        <button
                                            type="submit"
                                            className="btn btn-primary rounded-pill float-right mt-3"
                                            disabled={!(isValid && dirty)}
                                        >
                                            Ajouter l'évènement
                                        </button>
                                    </Form>
                                );
                            }}
                        </Formik>
                    </div>
                </div>
            </div>
        </section>
    );
}
