import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import {
    getEventById,
    addParticipant,
    removeParticipant,
    eventTasks,
} from "../../services/event";
import EventTaskItem from "../../components/Event/EventTaskItem";
import EventDetails from "../../components/Event/EventDetails";
import "../../components/Event/styles.scss";
import { IN, OUT, BY_MEALS, BY_DRINKS, GET_KEYS } from "../../constants";

const EventShow = ({ match }) => {
    const { auth, team } = useSelector((state) => state);
    const event_id = match.params.event_id;
    const [render, setRender] = useState(true);

    const [event, setEvent] = useState("");
    const [eventDetails, setEventDetails] = useState([]);
    const [playersIn, setPlayersIn] = useState([]);
    const [playersOut, setPlayersOut] = useState([]);
    const [inChargeMeals, setInChargeMeals] = useState([]);
    const [inChargeDrinks, setInChargeDrinks] = useState([]);
    const [inChargeKeys, setInChargeKeys] = useState([]);

    useEffect(() => {
        let isMounted = true;
        if (render || isMounted) getEvent(event_id);
        return (isMounted = false);
    }, [render, event_id]);

    const getEvent = (event_id) => {
        getEventById(event_id).then((res) => {
            if (res.status === 200) {
                setEvent(res.data.data.event);
                setEventDetails(res.data.data.event_details);
                setPlayersIn(res.data.data.players_in);
                setPlayersOut(res.data.data.players_out);
                setInChargeMeals(res.data.data.players_ico_meals);
                setInChargeDrinks(res.data.data.players_ico_drinks);
                setInChargeKeys(res.data.data.players_ico_keys);
                setRender(false);
            }
        });
    };

    const handleAttendeesAction = (e, type) => {
        e.preventDefault();
        switch (type) {
            case IN:
                addParticipant(event_id, team.team_id).then(() =>
                    setRender(true)
                );
                break;
            case OUT:
                removeParticipant(event_id, team.team_id).then(() =>
                    setRender(true)
                );
                break;
            default:
                break;
        }
    };

    const disableBtn = (players) => {
        return players.some((player) => player.id === auth.user.id);
    };

    const handlePlayerTasksQS = (type, action) => {
        return `?type=${type}&action=${action}`;
    };

    const handlePlayerTasksResponse = (res, type) => {
        switch (type) {
            case BY_MEALS:
                setInChargeMeals(res.users);
                break;
            case BY_DRINKS:
                setInChargeDrinks(res.users);
                break;
            case GET_KEYS:
                setInChargeKeys(res.users);
                break;
            default:
                return;
        }
    };

    const handleEventTasks = (e, type, action) => {
        e.preventDefault();
        let query = handlePlayerTasksQS(type, action);

        eventTasks(event_id, query).then((res) => {
            if (res.status === 200) {
                handlePlayerTasksResponse(res.data.data, res.data.data.type);
                setRender(true);
            }
        });
    };

    const eventDetailsItems = [
        {
            classes: "",
            subTitle: "Responsable salle",
            players: inChargeKeys,
            type: GET_KEYS,
        },
        {
            classes: "mt-5",
            subTitle: "Responsable repas",
            players: inChargeMeals,
            type: BY_MEALS,
        },
        {
            classes: "mt-5",
            subTitle: "Responsable boisson",
            players: inChargeDrinks,
            type: BY_DRINKS,
        },
    ];

    return (
        <section className="main-section">
            <div className="container">
                <div className="row pb-5">
                    <EventDetails
                        event={event}
                        playersIn={playersIn}
                        playersOut={playersOut}
                        handleAttendeesAction={handleAttendeesAction}
                        disableBtn={disableBtn}
                        eventDetails={eventDetails}
                    />

                    <div className="col-lg-4 offset-lg-1 mt-5 mt-lg-0">
                        <div className="card py-4">
                            <div className="card-body">
                                {eventDetailsItems.map((item, i) => {
                                    return (
                                        <EventTaskItem
                                            key={i}
                                            handleEventTasks={handleEventTasks}
                                            connectedUser={auth.user}
                                            players={item.players}
                                            subTitle={item.subTitle}
                                            type={item.type}
                                            classes={item.classes}
                                            event={event}
                                            eventDetails={eventDetails}
                                            team={team}
                                            disableBtn={disableBtn}
                                            playersOut={playersOut}
                                        />
                                    );
                                })}
                            </div>
                        </div>

                        <div className="card mt-5">
                            <div className="card-header bg-transparent pt-4 px-4 pb-0">
                                <h5 className="font-weight-bold mb-3">
                                    Commentaires
                                </h5>
                            </div>
                            <div className="card-body">
                                <p>Event's Feeds will be displayed here ...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default EventShow;
