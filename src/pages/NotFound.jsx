import React from 'react';
import { Link } from 'react-router-dom';

export default function NotFound(){
    const style={
        maxWidth:'400px',
        maxHeight: '200px'
    }
    return(
        <section>
            <div className="container vh-100">
                <div className="row justify-content-center align-items-center vh-100">
                    <div className="text-center">
                        <img src="/assets/svg/not_found.svg" alt="" style={style}/>
                        <div className="mt-2">
                            <h1 className="display-2 font-weight-bold text-grey">404</h1>
                            <h3 className="font-weight-bold text-grey">Page Non Trouvé</h3>
                            <Link to="/home" className="btn btn-primary mt-5">Retour à l'accueil</Link>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}