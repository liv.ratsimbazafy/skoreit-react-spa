import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";


export default function LandingPage() {
    const {authenticated, user} = useSelector(state => state.auth);
    const history = useHistory();
    
   
    useEffect(() => {
        if(authenticated && user.email_verified_at !== "" && user.team_id !== "")
            history.push('/home');
    }, [history, authenticated, user]);

    const style={
        maxWidth:'600px',
        maxHeight: '400px'
    }
    return (
        <section className="landing">
            <div className="container vh-100">
                <div className="row vh-100 justify-content-center align-items-center mt-md-0">
                    <div className="col-lg-5 ">
                        <h1 className=" text-black font-weight-normal text-center text-md-left" style={{fontFamily: "Gtek", letterSpacing: "2px"}}>atlet</h1>
                        <h4 className="d-none d-md-block mt-2">L'organisation sportive rendue simple .</h4>                       
                        <div className="text-center text-md-left d-none d-md-block mt-3">
                            <Link to="/signup" className="btn btn-primary rounded-pill btn-lg mt-5">Je commence</Link>
                        </div>

                        <div className="d-flex flex-column d-block d-md-none" style={{marginTop: "7rem"}}>
                            <Link to="/signup" className="btn btn-primary text-white py-3 rounded-pill btn-lg">Créer un compte</Link>
                            <Link to="/signin" className="btn btn-black py-3 rounded-pill btn-lg mt-3">Se connecter</Link>
                        </div>
                        
                    </div>
                    <div className="col-lg-7 text-center d-none d-lg-block">
                        <img src="/assets/svg/fans.svg" alt="" style={style}/>
                    </div>
                </div>
            </div>
        </section>
    );
}
