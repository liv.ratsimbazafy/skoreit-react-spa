import React, { Fragment, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import TeamPlayerStatCard from "../components/Team/TeamPlayerStatCard";
import TeamCard from "../components/Team/TeamCard";
import { formatTime } from "../helpers";
import { format } from "date-fns";
import { BsWatch } from "react-icons/bs";
import SimpleCard from "../components/Team/SimpleCard";
import noData from "../assets/no_data.svg";

export default function Dashboard({ teamInfos, events, userData }) {
    const [passedEvents, setPassedEvents] = useState("");
    const [upcomingEvents, setUpcomingEvents] = useState("");

    useEffect(() => {
        let isMounted = true;
        if (isMounted) {
            setUpcomingEvents(events.upcoming_events);
            setPassedEvents(events.passed_events);
        }
        return (isMounted = false);
    }, [events]);

    const WeeklyComingEvent = () => {
        let event = [];

        if (upcomingEvents && upcomingEvents.length)
            [event] = upcomingEvents.slice(0, 1);

        return (
            <div
                className="card card-bgi rounded-lg"
                style={{
                    backgroundImage: `url("https://wallpaperaccess.com/full/820979.jpg")`,
                }}
            >
                <div className="card-bgi-box">
                    <h1 className="text-capitalize font-weight-light">
                        {event.venue && format(new Date(event.venue), "MMM")}
                    </h1>
                    <p className="text-capitalize mt-n3 display-3 font-weight-light">
                        {event.venue && format(new Date(event.venue), "dd")}
                    </p>
                    <p className=" text-center mt-n2 h5 font-weight-light d-flex justify-content-center align-items-center">
                        <BsWatch className="" />
                        <span className="">
                            {event.time && formatTime(event.time, "long")}
                        </span>
                    </p>
                    <Link
                        to={`/event/show/${event.id}`}
                        className="btn btn-outline-light rounded-pill mt-3"
                    >
                        Voir l'évènement
                    </Link>
                </div>
            </div>
        );
    };

    const AllTeamEvents = () => {
        return (
            <SimpleCard
                header="Tous les évènements"
                total={passedEvents.length}
            >
                <ul className="list-group list-group-flush">
                    {passedEvents &&
                        passedEvents.data.slice(0, 3).map((event) => {
                            return (
                                <li
                                    key={event.id}
                                    className="list-group-item d-flex justify-content-between align-items-center border-bottom"
                                >
                                    <span className="badge badge-primary badge-pill p-2">
                                        {event.venue &&
                                            format(
                                                new Date(event.venue),
                                                "dd MMM"
                                            )}{" "}
                                    </span>
                                    <Link to={`/event/show/${event.id}`}>
                                        <small className="text-dark font-weight-bold">
                                            {event.name}
                                        </small>
                                    </Link>
                                </li>
                            );
                        })}
                </ul>
                <Link
                    to={`/events`}
                    className="btn btn-primary_l_700 text-primary shadow-none mt-5 float-right"
                >
                    Voir les Évènements
                </Link>
            </SimpleCard>
        );
    };

    const EventsSection = () => {
        return (
            <section className="mt-5">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12">
                            <h3 className="font-weight-bold text-black">
                                Évènements
                            </h3>
                        </div>
                    </div>
                </div>

                <div className="container mt-3">
                    <div className="row">
                        <div className="col-md-6">
                            {upcomingEvents && upcomingEvents.length ? (
                                <WeeklyComingEvent />
                            ) : (
                                <NoEvent title="Aucun évènement à venir !"/>
                            )}
                        </div>
                        <div className="col-md-6 mt-3 mt-md-0">
                            {passedEvents && passedEvents.data.length ? (
                                <AllTeamEvents />
                            ) : (
                                <NoEvent title="Aucun évènement passé !"/>
                            )}
                        </div>
                    </div>
                </div>
            </section>
        );
    };

    const NoEvent = ({title}) => {
        return (
            <div
                className="card rounded-lg shadow-sm"
                style={{ minHeight: "350px" }}
            >
                <div className="card-body d-flex flex-column justify-content-center align-items-center">
                    <img
                        src={noData}
                        alt=""
                        width="100"
                        height="100"
                    />
                    <p className="mt-3 text-primary_l_600 font-weight-bold">
                        {title}
                    </p>
                </div>
            </div>
        );
    };

    return (
        <Fragment>
            <TeamPlayerStatCard userData={userData} />
            <TeamCard team={teamInfos} />
            <EventsSection />
        </Fragment>
    );
}
