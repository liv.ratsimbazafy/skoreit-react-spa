import axios from 'axios';
import { store } from '../store';
import { sign_out } from '../pages/Auth/store/actions';


export const api = axios.create({   
    baseURL: process.env.REACT_APP_API_BASE_URL,
    withCredentials: true,
});

export const csrf = axios.create({    
    baseURL: process.env.REACT_APP_COOKIE_BASE_URL,
    withCredentials: true,
});

api.interceptors.response.use((res) => {
    return res;
}, (error) => {
    if (!error.response) {
        console.log("Please check your internet connection.");
        store.dispatch(sign_out())
        window.location.href = "/"
    }
    if(error.response.status === 401) {        
        store.dispatch(sign_out())
        window.location.href = "/"
        console.log("DISCONNECTED", error);
    }
    return Promise.reject(error)
})

csrf.interceptors.response.use((res) => {
    return res;
}, (error) => {
    if (!error.response) {
        console.log("Please check your internet connection.");
        let confirm = window.confirm('No internet connexion')
        if(confirm){
            return false;
        }
        return false;
    }
    // else if(error.response.status === 401) {        
    //     store.dispatch(sign_out())
    //     window.location.href = "/"
    //     console.log("DISCONNECTED", error);
    // }
    return Promise.reject(error)
})


