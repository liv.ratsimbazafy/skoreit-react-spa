import { api } from "../config";

export const getTeamEvents = async (team_id, page) => {
    return api.get(`/event/team/${team_id}?page=${page}`);
};

export const createEvent = async (event) => {
    return api.post("/event/create", event);
};

export const getEventById = async (event_id) => {
    return api.get(`/event/${event_id}`);
};

export const editEvent = async (event, event_id) => {
    return api.post(`/event/edit/${event_id}`, event);
};

export const deleteEvent = async (team_id, event_id) => {
    return api.delete(`/event/team/${team_id}/delete/${event_id}`);
};

export const addParticipant = async (event_id, team_id) => {
    return api.get(`/event/${event_id}/team/${team_id}/add-player`);
};

export const adminAddParticipant = async (event_id, team_id, player_id) => {
    return api.get(`/event/${event_id}/team/${team_id}/player/${player_id}/add`);
};
export const adminRemoveParticipant = async (event_id, team_id, player_id) => {
    return api.get(`/event/${event_id}/team/${team_id}/player/${player_id}/remove`);
};

export const removeParticipant = async (event_id, team_id) => {
    return api.get(`/event/${event_id}/team/${team_id}/remove-player`);
};

//@todo to refacto
export const getEventParticipants = async (reqData, headers) => {
    return api.post("/event/participant/all", reqData, headers);
};

export const paginatedEvent = async (teamId, pageNumber, headers) => {
    return api.get(`/event/${teamId}?page=${pageNumber}`, headers);
};

export const eventTasks = async (event_id, query) => {
    return api.get(`/event/${event_id}/tasks${query}`);
};

export const addContribution = async (event_id, request) => {
    return api.post(`/event/${event_id}/contribution/add`, request);
};

// export const addMealsAttendees = async (event_id, query) => {
//     return api.get(`/event/${event_id}/add-meals-attendees${query}`);
// };

// export const removeMealsAttendees = async (event_id) => {
//     return api.get(`/event/${event_id}/remove-meals-attendees`);
// };
