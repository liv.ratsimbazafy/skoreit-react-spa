import { api } from "../config";

export const typesAndCategories = async () => {
    return api.get("/sports");
};

export const teamCreate = async (team) => {
    return api.post("/team/create", team);
};

export const get_team_infos = async (team_id) => {
    return api.get(`/team/${team_id}`);
};

export const team_create_season = async (data, team_id) => {
    return api.patch(`/team/season/create/${team_id}`, data);
};

export const team_edit = async (team_id, data) => {
    return api.patch(`/team/edit/${team_id}`, data);
};

export const get_global_stats = async (team_id, page) => {
    return api.get(`/team/${team_id}/stats?page=${page}`);
};

export const teamUpdateContributionPrice = async (data) => {
    return api.post("/team/contribution/edit", data);
};
