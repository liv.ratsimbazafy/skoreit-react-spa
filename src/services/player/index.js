import { api } from "../config";

export const getTeamPlayers = async (team_id, page) => {
    return api.get(`/team/${team_id}/players?page=${page}`);
};

export const getPlayer = async (team_id, player_id) => {
    return api.get(`/team/${team_id}/player/${player_id}`);
};

export const addNewPlayers = async (players) => {
    return api.post(`/team/players/add`, players);
};

export const removePlayer = async (data) => {
    return api.post(`/team/player/remove`, data);
};

export const editPlayerDetails = async (data) => {
    return api.post(`/team/player/edit/details`, data);
};

export const editPlayerAccount = async (data) => {
    return api.post(`/team/player/edit/account`, data);
};
