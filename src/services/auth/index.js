import { api } from "../config";
import { csrf } from "../config";

export const startSession = async () => {
    return csrf.get("/sanctum/csrf-cookie");
};

export const authenticate = async user => {
    return api.post("/authenticate", user);
};

export const register = async user => {
    return api.post("/register", user);
};

export const logout = async () => {
    return csrf.post("/logout");
};

export const me = async () => {
    return api.get("/user");
};

export const resendEmailVerification = async () => {
    return csrf.get("/email/resend");
};

export const signUnauthenticatedUser = async user_id => {
    return api.post("/user/resign", user_id);
};

export const userPasswordUpdate = async user_data => {
    return api.post("/user/password/update", user_data);
};

export const userAvatarUpdate = async avatar => {
    return api.post("/user/avatar/update", avatar);
};
