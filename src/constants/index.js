export const IN = "IN";
export const OUT = "OUT";
export const ADD = "ADD";
export const REMOVE = "REMOVE";
export const BY_MEALS = "buy_meals";
export const BY_DRINKS = "buy_drinks";
export const GET_KEYS = "get_keys";